import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {fetchCalendarViewings} from '../../redux/calendar/calendar.actions';
import {selectViewingsOffCalendar} from '../../redux/calendar/calendar.selectors';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from "@fullcalendar/daygrid";
import { RowContainer } from '../../index.styles';
import Header from '../../components/header/header.component';
import { CalendarContainer } from './calendar-dashboard.styles';
import LoadingComponent from '../../components/with-spinner/loading.component';
import { createLoadingSelector } from '../../redux/loading/loading.selectors';
import { createStructuredSelector } from 'reselect';
import { format } from 'date-fns';

const loadingSelector = createLoadingSelector(['CALENDAR_VIEWINGS']);

const mapState = createStructuredSelector({
    viewings: selectViewingsOffCalendar,
    isLoading: loadingSelector
});

const mapDispatch = dispatch => ({
    fetchCalendarViewings: () => dispatch(fetchCalendarViewings())
});

 const Calendar = ({viewings, isLoading, fetchCalendarViewings}) => {

    useEffect(() => {
        fetchCalendarViewings();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const changedViewings = viewings.map(({ timeSlot: date, ...rest }) => ({ date, ...rest }));
    let convertTimes = changedViewings.map(item => ({
        ...item,
        date: format(item.date.seconds * 1000, 'yyyy-LL-dd')
    }))

    const handleEventClick = arg => {
        alert('asd')
    };

    if (isLoading) return <LoadingComponent />

    return (
        <CalendarContainer>
            <Header />
            <RowContainer className='calendar-container'>
                <FullCalendar 
                    defaultView="dayGridMonth"
                    header={{
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
                      }}
                    plugins={[dayGridPlugin]}
                    events={convertTimes}
                    eventClick={handleEventClick}
                />
            </RowContainer>
        </CalendarContainer>
    )
}

export default connect(mapState, mapDispatch)(Calendar);