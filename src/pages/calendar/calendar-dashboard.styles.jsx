import styled from 'styled-components';

export const CalendarContainer = styled.div`
    .calendar-container {
        background: ${props => props.theme.white};
    }
`