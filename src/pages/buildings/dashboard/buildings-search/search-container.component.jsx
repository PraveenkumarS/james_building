import React from 'react';
import { Grid } from 'semantic-ui-react';
import { PageDashboardContainer } from '../../../../components/page-dashboard/page-dashboard.styles';
import BuildingFilters from './buildings-filters.component';
import PageDashResults from '../../../../components/page-dashboard/page-dash-results.component';

const BuildingSearchContainer = ({
    countries, 
    initialFetched, 
    applyFilters, 
    columns,
    building_types
}) => {
    return (
        <PageDashboardContainer>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={3}>
                        <BuildingFilters
                            countries={countries} 
                            applyFilters={applyFilters}
                            building_types={building_types} 
                        />
                    </Grid.Column>
                    <Grid.Column width={13}>
                        <PageDashResults initialFetched={initialFetched} columns={columns} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </PageDashboardContainer>
    );
}

export default BuildingSearchContainer;