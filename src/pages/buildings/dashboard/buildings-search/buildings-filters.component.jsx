import React, { useState, useEffect, Fragment } from 'react';
import { Form, Field } from 'react-final-form';
import { Button } from 'semantic-ui-react';

const BuildingFilters = ({ countries, applyFilters, building_types }) => {

    const [objectCondition, setObjectCondition] = useState(false);
    const [countrySelected, setCountrySelected] = useState('');
    const [countryBoolean, setCountryBoolean] = useState(false);
    const [citySelected, setCitySelected] = useState('');
    const [cityBoolean, setCityBoolean] = useState(false);
    const [uncheckBTRadio, setUncheckBTRadio] = useState('');
    const [uncheckBURadio, setUncheckBURadio] = useState('');
    const [uncheckCountryRadio, setUncheckCountryRadio] = useState(null);
    const [uncheckCityRadio, setUncheckCityRadio] = useState(null);
    const [uncheckAreaRadio, setUncheckAreaRadio] = useState(null);

    useEffect(() => {
        if (typeof Object.keys(building_types) !== 'undefined' && Object.keys(building_types).length > 0) {
            setObjectCondition(true);
        } else {
            setObjectCondition(false);
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleSubmit = async values => {
        applyFilters(values);
    };

    return (
        <div className="page-dash-filters-form">
            <div className="filters-section">
                <h2>Filters</h2>
            </div>

            <Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                    <form onSubmit={handleSubmit}>

                        {objectCondition ? (
                            <Fragment>
                                <div className="filters-section">
                                    <h3>Building Types</h3>
                                    {Object.keys(building_types['building types']).map((building_type, index) => (
                                        <div key={index}>
                                            <Field
                                                name="building_type"
                                                id={`building_type-${index}`}
                                                component="input"
                                                type="radio"
                                                value={building_type}
                                                checked={uncheckBTRadio === building_type}
                                                onClick={() => uncheckBTRadio === building_type ? setUncheckBTRadio('') : setUncheckBTRadio(building_type)}
                                            />
                                            <label htmlFor={`building_type-${index}`}>{building_type}</label>
                                        </div>
                                    ))}
                                </div>
                                <div className="filters-section">
                                    <h3>Building Units</h3>
                                    {building_types && Object.keys(building_types['building units']).map((building_unit, index) => (
                                        <div key={index}>
                                            <Field
                                                name="building_unit"
                                                id={`building_unit-${index}`}
                                                component="input"
                                                type="radio"
                                                value={building_unit}
                                                checked={uncheckBURadio === building_unit}
                                                onClick={() => uncheckBURadio === building_unit ? setUncheckBURadio('') : setUncheckBURadio(building_unit)}
                                            />
                                            <label htmlFor={`building_unit-${index}`}>{building_unit}</label>
                                        </div>
                                    ))}
                                </div>
                            </Fragment>
                        ) : (
                                null
                            )}

                        <div className="filters-section">
                            <h3>Country</h3>
                            {Object.keys(countries).map((country, index) => (
                                <div key={index}>
                                    <Field
                                        name="country"
                                        id={`country-${index}`}
                                        component="input"
                                        type="radio"
                                        value={country}
                                        checked={uncheckCountryRadio === country}
                                        onClick={() => uncheckCountryRadio === country ?
                                            (setUncheckCountryRadio(null),
                                                setCountrySelected(''),
                                                setCountryBoolean(false),
                                                setCitySelected(''),
                                                setCityBoolean(false))
                                            :
                                            (setUncheckCountryRadio(country),
                                                setCountrySelected(country),
                                                setCountryBoolean(true),
                                                setCitySelected(''),
                                                setCityBoolean(false))
                                        }
                                    />
                                    <label htmlFor={`country-${index}`}>{country}</label>
                                </div>
                            ))}
                        </div>

                        {countryBoolean ? (
                            <div className="filters-section">
                                <h3>City</h3>
                                {Object.keys(countries[countrySelected]).map((city, index) => (
                                    <div key={index}>
                                        <Field
                                            name="city"
                                            id={`city-${index}`}
                                            component="input"
                                            type="radio"
                                            value={city}
                                            checked={uncheckCityRadio === city}
                                            onClick={() => uncheckCityRadio === city ?
                                                (setUncheckCityRadio(null),
                                                    setCitySelected(''),
                                                    setCityBoolean(false))
                                                :
                                                (setUncheckCityRadio(city),
                                                    setCitySelected(city),
                                                    setCityBoolean(true))
                                            }
                                        />
                                        <label htmlFor={`city-${index}`}>{city}</label>
                                    </div>
                                ))}
                            </div>
                        ) : (
                                null
                            )
                        }
                        {cityBoolean ? (
                            <div className="filters-section">
                                <h3>City</h3>
                                {Object.keys(countries[countrySelected][citySelected]).map((area, index) => (
                                    <div key={index}>
                                        <Field
                                            name="area"
                                            id={`area-${index}`}
                                            component="input"
                                            type="radio"
                                            value={area}
                                            checked={uncheckAreaRadio === area}
                                            onClick={() => uncheckAreaRadio === area ?
                                                setUncheckAreaRadio(null)
                                                :
                                                setUncheckAreaRadio(area)
                                            }
                                        />
                                        <label htmlFor={`area-${index}`}>{area}</label>
                                    </div>
                                ))}
                            </div>
                        ) : (
                                null
                            )
                        }
                        <Button type="submit" disabled={submitting || pristine}>
                            Filter now
                    </Button>
                    </form>
                )}
            />
        </div>
    );
}

export default BuildingFilters;

// Search on filter click?

/*
   <div className="filters-section">
        <h3>Country</h3>
        {Object.keys(countries).map((country, index) => (
            <div key={index}>
                <Field
                    name="country"
                    id={`country-${index}`}
                    component="input"
                    type="radio"
                    value={country}
                    checked={uncheckRadio}
                    onClick={() => uncheckRadio ? setUncheckRadio(false) : setUncheckRadio(true)}
                />
                <label htmlFor={`country-${index}`}>{country}</label>
            </div>
        ))}
    </div>




                            onChange={countryBoolean ? () => {
                                        setCountrySelected('');
                                        setCountryBoolean(false);
                                        setCitySelected('');
                                        setCityBoolean(false);
                                    }
                                    : () => {
                                        setCountrySelected(country);
                                        setCountryBoolean(true);
                                        setCitySelected('');
                                        setCityBoolean(false);
                                        changeRadio(country);
                                    }}


 <OnChange name="city">
                        {(value) => {
                            if (cityBoolean === false) {
                                setCitySelected(value);
                                setCityBoolean(true);
                            } else {
                                setCitySelected('');
                                setCityBoolean(false);
                            }
                        }}
                    </OnChange>
*/