import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingsOffState } from '../../../redux/buildings/building.selectors';
import { getCountryList, getBuildingTypes } from '../../../redux/general/general.actions';
import { selectCountriesOffState, selectBuildingTypesOffState } from '../../../redux/general/general.selectors';
import { createLoadingSelector } from '../../../redux/loading/loading.selectors';
import { createStructuredSelector } from 'reselect';
import Header from '../../../components/header/header.component';
import { RowContainerFull } from '../../../index.styles';
import { PageHeader } from '../../../components/page-dashboard/page-dashboard.styles';
import { Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import LoadingComponent from '../../../components/with-spinner/loading.component';
import BuildingSearchContainer from './buildings-search/search-container.component';

const loadingSelector = createLoadingSelector(['FETCH_BUILDINGS', 'COUNTRIES_DOCUMENT', 'BUILDING_TYPES_DOCUMENT']);

const mapState = createStructuredSelector({
    buildings: selectBuildingsOffState,
    countries: selectCountriesOffState,
    building_types: selectBuildingTypesOffState,
    isLoading: loadingSelector
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values)),
    getCountryList: () => dispatch(getCountryList()),
    getBuildingTypes: () => dispatch(getBuildingTypes())
});

const columns = ['name', 'area', 'city', 'country', 'units', 'rooms', 'developer'];

const BuildingsDashboard = ({
    buildings,
    applyBuildingFilters,
    fetchBuildingsStart,
    isLoading,
    getCountryList,
    getBuildingTypes,
    countries,
    building_types
}) => {
    useEffect(() => {
        fetchBuildingsStart();
        getCountryList();
        getBuildingTypes();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const applyFilters = (values) => {
        applyBuildingFilters(values);
    }

    if (isLoading) return <LoadingComponent />

    return (
        <Fragment>
            <Header />
            <RowContainerFull>
                <PageHeader>
                    <div className="copy dashboard">
                        <Icon name="building outline" />
                        <div className="header">
                            <h1>Buildings</h1>
                        </div>
                    </div>
                    <div className="stats">
                        <div className="first stat">
                            <span className="title">Total</span>
                            <span className="number">21</span>
                        </div>
                        <div className="second stat">
                            <span className="title">Available Units</span>
                            <span className="number">64</span>
                        </div>
                    </div>
                    <div className="buttons">
                        <Button as={Link} to='/building-create' className="create">
                            Create Building
                        </Button>
                    </div>
                </PageHeader>
                <BuildingSearchContainer
                    initialFetched={buildings}
                    countries={countries}
                    building_types={building_types}
                    columns={columns}
                    applyFilters={applyFilters}
                />
            </RowContainerFull>
        </Fragment>
    );
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);



/*
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingList } from '../../../redux/buildings/building.selectors';
import { createStructuredSelector } from 'reselect';

import Header from '../../../components/header/header.component';
import { RowContainer } from '../../../index.styles';
import { Button } from 'semantic-ui-react';
import PageDashboard from '../../../components/page-dashboard/page-dashboard.component';



const mapState = createStructuredSelector({
    buildings: selectBuildingList
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values))
});

const filters = [
    { name: 'city', innerArray: ['Manchester', 'London', 'Bristol']},
    { name: 'area', innerArray: ['Shoreditch', 'Castlefield', 'Northern Quarter']}
];

class BuildingsDashboard extends Component {

    componentDidMount() {
        const { fetchBuildingsStart } = this.props;
        fetchBuildingsStart();
    }

    applyFilters = (values) => {
        const { applyBuildingFilters } = this.props;
        applyBuildingFilters(values);
        console.log('apply filters function');
    }

    render() {
        const { buildings } = this.props;
        return (
            <Fragment>
                <Header />
                <RowContainer>
                    <div className="add-button">
                        <Button>Add Property</Button>
                    </div>
                    <PageDashboard initialFetched={buildings} filters={filters} applyFilters={this.applyFilters} />
                </RowContainer>
            </Fragment>
        );
    }
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);
*/