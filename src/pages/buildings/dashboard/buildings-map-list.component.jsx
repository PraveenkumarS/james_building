import React from 'react';
import { Link } from 'react-router-dom';

const BuildingsMapList = ({buildings}) => {
    return (
        <div>
            {buildings.map(building => (
                <div key={building.id}>
                    <Link to={`/buildings/${building.id}`}>{building.id}</Link>
                </div>
            ))}
        </div>
    ); 
}

export default BuildingsMapList;