import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { selectBuilding, buildingPhotoList } from '../../../redux/buildings/building.selectors';
import {singleBuildingStart, fetchBuildingPhotos, deleteBuilding} from '../../../redux/buildings/building.actions';
import { createStructuredSelector } from 'reselect';
import { createLoadingSelector } from '../../../redux/loading/loading.selectors';
import { Link } from 'react-router-dom';
import { BuildingDetailWrapper } from './building-detail.styles';
import { PageHeader } from '../../../components/page-dashboard/page-dashboard.styles';
import { Icon, Button, Tab, Dropdown } from 'semantic-ui-react';
import Header from '../../../components/header/header.component';
import { RowContainerFull } from '../../../index.styles';
import TabbedArea from '../../../components/page-detail/page-detail-tabbed.component';
import BuildingTabOne from './building-tabbed/building-tab-one.component';
import BuildingTabTwo from './building-tabbed/building-tab-two.component';
import BuildingTabThree from './building-tabbed/building-tab-three.component';
import BuildingTabSix from './building-tabbed/building-tab-six.component';
import LoadingComponent from '../../../components/with-spinner/loading.component';
import NotFound from '../../../components/not-found/not-found.component';

const loadingSelector = createLoadingSelector(['SINGLE_BUILDING', 'BUILDING_PHOTOS']);

const mapState = createStructuredSelector({
    building: selectBuilding,
    photos: buildingPhotoList,
    isLoading: loadingSelector
});

const mapDispatch = dispatch => ({
    singleBuildingStart: (buildingId) => dispatch(singleBuildingStart(buildingId)),
    fetchBuildingPhotos: (buildingId) => dispatch(fetchBuildingPhotos(buildingId)),
    deleteBuilding: (buildingId) => dispatch(deleteBuilding(buildingId))
});

const BuildingDetail = ({
    building, 
    photos, 
    singleBuildingStart, 
    fetchBuildingPhotos, 
    match, 
    isLoading,
    deleteBuilding
}) => {

    const buildingId = match.params.id;
    useEffect(() => { 
            singleBuildingStart(buildingId);
            fetchBuildingPhotos(buildingId);
    }, []);  // eslint-disable-line react-hooks/exhaustive-deps

    const tabbedPanes = [
        {
          menuItem: 'Summary',
          render: () => <Tab.Pane attached={false}><BuildingTabOne item={building} photos={photos} /></Tab.Pane>,
        },
        {
          menuItem: 'Building Info',
          render: () => <Tab.Pane attached={false}><BuildingTabTwo item={building} /></Tab.Pane>,
        },
        {
          menuItem: 'Documents',
          render: () => <Tab.Pane attached={false}><BuildingTabThree item={building} /></Tab.Pane>,
        },
        {
            menuItem: 'Notes',
            render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        },
        {
            menuItem: 'Units',
            render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        },
        {
            menuItem: 'Rooms',
            render: () => <Tab.Pane attached={false}><BuildingTabSix item={building} /></Tab.Pane>,
        },
        {
            menuItem: 'Tenancies',
            render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        },
        {
            menuItem: 'Members',
            render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        },
        {
            menuItem: 'Tasks',
            render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane>,
        }
    ];

    if (isLoading) return <LoadingComponent />
    if (Object.keys(building).length === 0) return <NotFound />;

    return (
        
        <BuildingDetailWrapper>
            <Header />
            <RowContainerFull>
                <PageHeader>
                    <div className="copy">
                        <Icon name="building outline" />
                        <div className="header">
                            <span>Building</span>
                            <h1>{building.name}</h1>
                        </div>
                    </div>
                    <div className="stats">
                        <div className="first stat">
                            <span className="title">Units</span>
                            <span className="number">{building.units}</span>
                        </div>
                        <div className="second stat">
                            <span className="title">Rooms</span>
                            <span className="number">{building.rooms}</span>
                        </div>
                    </div>
                    <div className="buttons">
                        <Button as={Link} to='/building-create' className="create">
                            Create Building
                        </Button>
                        <Button as={Link} to={`/building-edit/${building.id}`} className="edit">
                            Edit
                        </Button>
                        <Dropdown className="delete" text='More'>
                            <Dropdown.Menu>
                                <Dropdown.Item text='Delete' onClick={() => deleteBuilding(building.id)} />
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </PageHeader>
                <TabbedArea panes={tabbedPanes} item={building} />
            </RowContainerFull>           
        </BuildingDetailWrapper>
    );
}

export default connect(mapState, mapDispatch)(BuildingDetail);

// import { createStructuredSelector } from 'reselect';
// const mapState = createStructuredSelector({
//     buildings: selectBuildingList
// })

// const mapState = (state, ownProps) => {
//     const buildingId = ownProps.match.params.id;
//     let building = {};
//     if (state.buildings.buildings && state.buildings.buildings.length > 0) {
//         building = state.buildings.buildings.filter(building => building.id === buildingId)[0] || {};
//     }
//     return {
//         building,
//         rooms: state.buildings.buildingRooms
//     };
// };
