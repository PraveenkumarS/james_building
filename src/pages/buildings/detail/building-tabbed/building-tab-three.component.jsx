import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import {fetchBuildingDocuments} from '../../../../redux/buildings/building.actions';
import { buildingDocumentList } from '../../../../redux/buildings/building.selectors';
import { createStructuredSelector } from 'reselect';
import {createLoadingSelector} from '../../../../redux/loading/loading.selectors';
import { SingleTab } from '../../../../components/page-detail/page-detail-tab.styles';
import { Table, Icon } from 'semantic-ui-react';
import LoadingComponent from '../../../../components/with-spinner/loading.component';

const loadingSelector = createLoadingSelector(['BUILDING_DOCUMENTS']);

const mapState = createStructuredSelector({
    documents: buildingDocumentList,
    isLoading: loadingSelector
});

const mapDispatch = dispatch => ({
    fetchBuildingDocuments: (buildingId) => dispatch(fetchBuildingDocuments(buildingId))
});

const columns = ['Name', 'Added By', 'Document Type', 'File Type', 'Created At'];


const BuildingTabThree = ({item, fetchBuildingDocuments, documents, isLoading}) => {

    useEffect(() => {
        fetchBuildingDocuments(item.id);
    }, [fetchBuildingDocuments, item.id]);

    if (isLoading) return <LoadingComponent />
    return (
        <SingleTab>
            <div className="documents">  
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            {columns.map((column, index) => (
                                <Table.HeaderCell key={index}>{column}</Table.HeaderCell>
                            ))}
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {documents && documents.map((document, index) => (
                            <Table.Row key={index}>
                                <Table.Cell>
                                    <Icon name={document.icon} />
                                    {document.name}
                                </Table.Cell>
                                <Table.Cell>{document.id}</Table.Cell>
                                <Table.Cell><a href={document.url} target="_blank" rel="noopener noreferrer">View</a></Table.Cell>
                            </Table.Row>
                        ))}

                    </Table.Body>
                </Table>
            </div>
        </SingleTab>
    );
}

export default connect(mapState, mapDispatch)(BuildingTabThree);