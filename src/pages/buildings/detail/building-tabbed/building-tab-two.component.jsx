import React from 'react';
import { SingleTab } from '../../../../components/page-detail/page-detail-tab.styles';
import { Grid } from 'semantic-ui-react';

const BuildingTabTwo = ({item}) => {
    return (
        <SingleTab>
            <div className="details">
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <span className="top">Maximum Occupany:</span>
                            <span className="bottom">
                                72
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <span className="top">Floors:</span>
                            <span className="bottom">
                                11
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                        <span className="top">Concierge contact:</span>
                            <span className="bottom">
                                concierge@concierge.com
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <span className="top">Features:</span>
                            <span className="bottom">
                                <ul>
                                    <li>
                                        Concierge
                                    </li>
                                    <li>
                                        Roof terrace
                                    </li>
                                    <li>
                                        Gym
                                    </li>
                                    <li>
                                        Pool
                                    </li>
                                    <li>
                                        Lifts
                                    </li>
                                    <li>
                                        Coffee shop
                                    </li>
                                </ul>
                            </span>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </SingleTab>
    );
}

export default BuildingTabTwo;