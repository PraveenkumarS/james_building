import React from 'react';
import { SingleTab } from '../../../../components/page-detail/page-detail-tab.styles';
import { Grid } from 'semantic-ui-react';
import BuildingTabPhotos from './building-tab-photos.component';


const BuildingTabOne = ({item, photos}) => {
    return (
        <SingleTab>
            <div className="details">
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <span className="top">Address:</span>
                            <span className="bottom">
                                {item.firstLineAddress},<br />
                                {item.area},<br />
                                {item.city},<br />
                                {item.postcode}
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <span className="top">Area:</span>
                            <span className="bottom">
                                {item.area}
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <span className="top">City:</span>
                            <span className="bottom">
                                {item.city}
                            </span>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <span className="top">Developer name:</span>
                            <span className="bottom">
                                {item.developer}
                            </span>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <BuildingTabPhotos item={item} photos={photos} />
            </div>
        </SingleTab>
    );
}

export default BuildingTabOne;