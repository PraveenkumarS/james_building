import React from 'react';
import { Image } from 'semantic-ui-react';

const BuildingTabPhotos = ({photos}) => {
    return (
        <div className="detail-photos">
            <span className="top">Photos:</span>
            <div className="photos">
                {photos && photos.map((photo, index) => (
                    <Image key={index} src={photo} />
                ))}
            </div>
        </div>
    );
}

export default BuildingTabPhotos;

/*
Minify images on upload 
Photos in carousel and lightbox
*/

