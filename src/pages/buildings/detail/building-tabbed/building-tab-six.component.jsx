import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { buildingRoomList } from '../../../../redux/buildings/building.selectors';
import { buildingRoomsStart } from '../../../../redux/buildings/building.actions';
import { createStructuredSelector } from 'reselect';
import { SingleTab } from '../../../../components/page-detail/page-detail-tab.styles';
import { Table } from 'semantic-ui-react';
import { Link } from 'react-router-dom';


const mapState = createStructuredSelector({
    rooms: buildingRoomList
});

const mapDispatch = dispatch => ({
    buildingRoomsStart: (buildingId) => dispatch(buildingRoomsStart(buildingId))
})


const BuildingTabSix = ({ item, buildingRoomsStart, rooms }) => {

    useEffect(() => {
        buildingRoomsStart(item.id);
    }, [buildingRoomsStart, item.id]); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <SingleTab>
            <div className="building-rooms">
                <Table celled striped>
                    <Table.Body>
                        {rooms && rooms.map((room, index) => (
                            <Table.Row key={index}>
                                <Table.Cell width={4}><Link to={`/rooms/${room.id}`}>{room.name}</Link></Table.Cell>
                                <Table.Cell>{room.area}</Table.Cell>
                                <Table.Cell>{room.city}</Table.Cell>
                                <Table.Cell>{room.country}</Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
            </div>
        </SingleTab>
    );
}

export default connect(mapState, mapDispatch)(BuildingTabSix);