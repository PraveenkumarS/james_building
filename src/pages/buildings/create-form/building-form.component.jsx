/* eslint-disable jsx-a11y/alt-text */
import React, { Fragment, useEffect, useState, useCallback } from 'react';
import { connect } from 'react-redux';
import { singleBuildingStart, createBuilding } from '../../../redux/buildings/building.actions';
import { getCountryList } from '../../../redux/general/general.actions';
import { selectCountriesOffState } from '../../../redux/general/general.selectors';
import { createLoadingSelector } from '../../../redux/loading/loading.selectors';
import { Form, Field } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';
import Header from '../../../components/header/header.component';
import { PageHeader } from '../../../components/page-dashboard/page-dashboard.styles';
import { RowContainerFull } from '../../../index.styles';
import { Button, Icon, Grid } from 'semantic-ui-react';
import { PageForm } from '../../../components/page-form/page-form.styles';
import LoadingComponent from '../../../components/with-spinner/loading.component';
import NotFound from '../../../components/not-found/not-found.component';
import TextInput from '../../../components/page-form/text-input.component';
import TextArea from '../../../components/page-form/text-area.component';
import SelectInput from '../../../components/page-form/select-input.component';
import {imageUpload, deleteUploadImages} from '../../../firebase/firebase.js'; 
import {useDropzone} from 'react-dropzone';

//multiple image upload
// import ImagesUploader from 'react-images-uploader';
// import 'react-images-uploader/styles.css';
// import 'react-images-uploader/font.css';
//2

import ImageUploader from 'react-images-upload';
import { reducer } from 'react-redux-toastr';

const loadingSelector = createLoadingSelector(['SINGLE_BUILDING', 'COUNTRIES_DOCUMENT']);

const mapState = (state, ownProps) => {
    // console.log("ownProps",ownProps)
    const buildingId = ownProps.match.params.id;
    let building = {};
    if (state.buildings.building.building && state.buildings.building.building.id === buildingId) {
        building = state.buildings.building.building;
    }
    
    return {
        initialValues: building,
        ImageArray: building.photos || [],
        countries: selectCountriesOffState(state),
        isLoading: loadingSelector(state)
    };
};

const mapDispatch = dispatch => ({
    singleBuildingStart: (buildingId) => dispatch(singleBuildingStart(buildingId)),
    getCountryList: () => dispatch(getCountryList()),
    createBuilding: (values) => dispatch(createBuilding(values))
});
 
const BuildingCreateForm = ({
    match,
    initialValues,
    ImageArray,
    singleBuildingStart,
    getCountryList,
    createBuilding,
    countries,
    isLoading
}) => {

    const [countrySelected, setCountrySelected] = useState('');
    const [dragStartPos,setDragStartPos] = useState(null);
    const [timeStamp,setTimeStamp] = useState(Math.floor(Date.now() / 1000));
    const [imageState, setImageState] = useState(true);
    const [ imageArray, setImageArray] = useState(ImageArray);
    const [dragDropPos,setDragDropPos] = useState(null);
    const [countryBoolean, setCountryBoolean] = useState(false);
    const [citySelected, setCitySelected] = useState('');
    const [count, setCount] = useState(0);
    const [cityBoolean, setCityBoolean] = useState(false);
    const [areaBoolean, setAreaBoolean] = useState('')
    const [imageBoolean, setImageBoolean] = useState(false);
    const [imageSourceArray, setImageSourceArray] = useState([]);
    const [dragEndvalue, dragEnd] = useState('');
    const [DragEndBoolean, setDragEndBoolean] = useState(false);
  
    let buildingId = match.params.id;
    useEffect(() => {
        if (buildingId) {
            singleBuildingStart(buildingId);
        }
        getCountryList();

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    // const {acceptedFiles, rejectedFiles, getRootProps, getInputProps} = useDropzone({
    //     accept: 'image/jpeg, image/png'
    // });

    const onDrop = useCallback((event) => {
        console.log("imageFunction ", event)
        let image_Array = event;
        let imageArrayget = [];
        let imagesourceArray = imageSourceArray || [];
        let sourceArray = imageArray;

        for(let i=0; i<image_Array.length;i++){
            let check = imagesourceArray.filter( e=> e.name === image_Array[i].name)
            if(check.length === 0){
                imageArrayget.push(image_Array[i]);
            }
        }

        if((imageArray.length > 10) || (imageArray.length + event.length) > 10){
            alert("Not able to add more 10 Images ");
            return 0;
        }

        for(let i=0; i<imageArrayget.length;i++){
            imagesourceArray.push(imageArrayget[i])
                imageUpload(imageArrayget[i], timeStamp, (result)=>{
                    sourceArray.push(result);
                    setImageArray(sourceArray)
                    setCount(count + 1);
                    setImageState(false);
                    setTimeout(()=>{
                        setCount(count + 2);
                    }, 100)
            })
        }

        console.log('Arrray ', imagesourceArray, imageArray);
        setImageSourceArray(imagesourceArray);
    }, [])

    const {getRootProps, getInputProps} = useDropzone({onDrop})

    const handleSubmit = async values => {
        values.photos = imageArray;
        await createBuilding(values);
    };
   
    //dragstart
    const dragStart = event => {
        setDragStartPos(event.target.id);
        event.dataTransfer.effectAllowed = "copyMove";
        event.dataTransfer.setData('text/html', event); 
    }

    const dragLeave = event =>{
        event.preventDefault();
        console.log("dragLeave", event.target.id)  
        setDragDropPos();
        if(dragStartPos !== '' && event.target.id !== ''){
            let dragImage = imageArray[dragStartPos];
            let replaceImage = imageArray[event.target.id];
            imageArray[event.target.id] = dragImage;
            imageArray[dragStartPos] = replaceImage;
            setImageArray(imageArray);
            setCount(count + 1);
            setImageState(false);
        }
    }
 
    // drop current positioning
    const dropEndImage = (event) => {
        console.log("drop end ", dragStartPos, event.target.id)
        setDragDropPos();
        if(dragStartPos !== '' && event.target.id !== ''){
            let dragImage = imageArray[dragStartPos];
            let replaceImage = imageArray[event.target.id];
            imageArray[event.target.id] = dragImage;
            imageArray[dragStartPos] = replaceImage;
            setImageArray(imageArray);
            setCount(count + 1);
            setImageState(false);
        }
    }

    const imageFunctionBk = (event) => {
        let image_Array = event;
        let imageArrayget = [];
        let imagesourceArray = imageSourceArray || [];
        let sourceArray = imageArray;

        for(let i=0; i<image_Array.length;i++){
            let check = imagesourceArray.filter( e=> e.name === image_Array[i].name)
            if(check.length === 0){
                imageArrayget.push(image_Array[i]);
            }
        }

        if((imageArray.length > 10) || (imageArray.length + event.length) > 10){
            alert("Not able to add more 10 Images ");
            return 0;
        }

        for(let i=0; i<imageArrayget.length;i++){
            imagesourceArray.push(imageArrayget[i])
                imageUpload(imageArrayget[i], timeStamp, (result)=>{
                    sourceArray.push(result);
                    setImageArray(sourceArray)
                    setCount(count + 1);
                    setImageState(false);
            })
        }

        console.log('Arrray ', imagesourceArray, imageArray);
        setImageSourceArray(imagesourceArray);
    }

    // Delete firebase Image
     const deleteImage = (event, i) =>{
            console.log('deleteImage',  i)
            // Create a reference to the file to delete
            var getUrl = unescape(fileNameFromUrl(event));
            console.log("getUrl" ,getUrl)
            deleteUploadImages (getUrl, (result)=>{
                imageArray.splice(i,1);
                setImageState(false);
                setImageArray(imageArray);
                setCount(count+1);
            })        
    }
   
    function fileNameFromUrl(url) {
      var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
      if (matches.length > 1) {
        return matches[1];
      }
      return null;
    }

        // if (file) {
        //     var reader = new FileReader();
        //     reader.onloadend=()=>{
        //         console.log("hai",reader.result)
        //     }
        //     reader.readAsDataURL(file);

    const countryKeys = Object.keys(countries);

    //country
    let countryOptionsArray = [];
    for (let i = 0; i < countryKeys.length; i++) {
        let eachOption = {
            key: i, text: countryKeys[i], value: countryKeys[i]
        };
        countryOptionsArray.push(eachOption);
    }

    //city
    let cityOptionsArray = [];
    if (countryBoolean) {
        for (let i = 0; i < Object.keys(countries[countrySelected]).length; i++) {
            console.log("countries", countries)
            let eachOption = {
                key: i, text: Object.keys(countries[countrySelected])[i], value: Object.keys(countries[countrySelected])[i]
            };
            cityOptionsArray.push(eachOption);
        }
    }
    //area
    let areaOptionsArray = [];
    if (cityBoolean) {
        for (let i = 0; i < Object.keys(countries[countrySelected][citySelected]).length; i++) {
            let eachOption = {
                key: i, text: Object.keys(countries[countrySelected][citySelected])[i], value: Object.keys(countries[countrySelected][citySelected])[i]
            };
            areaOptionsArray.push(eachOption);
        }
    }

    if (isLoading) return <LoadingComponent />;
    if ((Object.keys(initialValues).length === 0) && (buildingId)) return <NotFound />;

    
    return (
        <Fragment>
            <Header />
            <RowContainerFull>
                <PageHeader>
                    <div className="copy form">
                        <Icon name="building outline" />
                        <div className="header">
                            <h1>Create Building</h1>
                        </div>
                    </div>
                </PageHeader>
                <PageForm>
                    <Form
                        onSubmit={handleSubmit}
                        initialValues={initialValues}
                        render={({ handleSubmit, form, submitting, pristine, values }) => (
                            <form onSubmit={handleSubmit} className="ui form">

                                <OnChange name="country">
                                    {(value) => {
                                        setCountrySelected(value);
                                        setCountryBoolean(true);
                                        setCitySelected('');
                                        setCityBoolean(false);
                                    }}
                                </OnChange>
                                <OnChange name="city">
                                    {(value) => {
                                        setCitySelected(value);
                                        setCityBoolean(true)
                                    }}
                                </OnChange>
                                <OnChange name="area">
                                    {(value) => {
                                        setAreaBoolean(value);
                                        setImageBoolean(true)
                                    }}
                                </OnChange>
                                {/* <onDragEnd name="dropend">
                                    {(value) => {
                                        dragEnd(value);
                                        setDragEndBoolean(true)
                                    }}
                                </onDragEnd> */}


                                <div className="radio-split building-units">
                                    <h2>What is the building type?</h2>
                                    <div className="radio-btn">
                                        <Field
                                            name="unit-choice"
                                            component="input"
                                            type="radio"
                                            value="multiple-units"
                                            id="multiple-units"
                                        />
                                        <label htmlFor="multiple-units">
                                            <Icon name="building outline" />
                                            <span>Building with multiple units</span>
                                        </label>
                                    </div>
                                    <div className="radio-btn">
                                        <Field
                                            name="unit-choice"
                                            component="input"
                                            type="radio"
                                            value="house-single-unit"
                                            id="house-single-unit"
                                        />
                                        <label htmlFor="house-single-unit">
                                            <Icon name="home" />
                                            <span>House with single unit</span>
                                        </label>
                                    </div>
                                    <div className="radio-btn">
                                        <Field
                                            name="unit-choice"
                                            component="input"
                                            type="radio"
                                            value="flat-single-unit"
                                            id="flat-single-unit"
                                        />
                                        <label htmlFor="flat-single-unit">
                                            <Icon name="home" />
                                            <span>Apartment with single unit</span>
                                        </label>
                                    </div>
                                </div>

                                <h2>Building details</h2>
                                <Grid columns={3}>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Field
                                                className="text-input"
                                                name="name"
                                                id="name"
                                                component={TextInput}
                                                type="text"
                                                placeholder="Building name"
                                            />
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Field
                                                className="text-input"
                                                name="address"
                                                component={TextArea}
                                                type="text"
                                                placeholder="Full address"
                                            />
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Field
                                                className="text-input"
                                                name="postcode"
                                                component={TextInput}
                                                type="text"
                                                placeholder="Postcode"
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Field
                                                name="country"
                                                type="text"
                                                component={SelectInput}
                                                options={countryOptionsArray}
                                                placeholder="Select Country"
                                            />
                                        </Grid.Column>
                                        <Grid.Column>
                                            {countryBoolean ? (
                                                <Field
                                                    name="city"
                                                    type="text"
                                                    component={SelectInput}
                                                    options={cityOptionsArray}
                                                    placeholder="Select city"
                                                />
                                            ) : (
                                                    <Field name="city" component="select">
                                                        <option value={initialValues.city}>{initialValues.city}</option>
                                                    </Field>
                                                )
                                            }
                                        </Grid.Column>
                                        <Grid.Column>
                                            {cityBoolean ? (
                                                <Field
                                                    name="area"
                                                    type="text"
                                                    component={SelectInput}
                                                    options={areaOptionsArray}
                                                    placeholder="Select area"
                                                />
                                            ) : (
                                                    <Field name="area" component="select">
                                                        <option value={initialValues.area}>{initialValues.area}</option>
                                                    </Field>
                                                )
                                            }
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                    </Grid.Row>
                                    {/* images upload */}
                                    <Grid.Row>

                                        <div className="dropZoneArea" {...getRootProps({className: 'dropzone'})}>
                                            <input {...getInputProps()} />
                                            <p>Drag 'n' drop some files here, or click to select files</p>
                                            <em>(Only *.jpeg and *.png images will be accepted)</em>
                                        </div>

                                        {/* <Grid.Column> */}
                                        {/* {imageBoolean ? ( */}
                                            {/* <ImageUploader
                                                withIcon={false}
                                                buttonText='Choose images'
                                                name="image"
                                                onChange={imageFunction}
                                                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                                label=""
                                            // withPreview={true}
                                            // fileSizeError="file size is too big"
                                            // maxFileSize={5242880}
                                            /> */}
                                            
                                        {/* ) : */}
                                            {/* (
                                                ""
                                            )

                                        } */}

                                        {/* </Grid.Column> */}
                                    </Grid.Row>
                                    <Grid.Row>
                                        {imageArray.length > 0 ?
                                            imageArray.map((items, i) =>
                                                // eslint-disable-next-line jsx-a11y/alt-text
                                            <div className="droptarget"
                                                key={i} 
                                                draggable={true}
                                                onDragStart={dragStart}
                                                id={i}
                                                data-position={i}
                                                onDragLeave={dropEndImage}
                                                onDrop={dragLeave}
                                             >
                                                    <img src={items}
                                                        className="imageDrop"
                                                        id={i}
                                                        name="images"
                                                        // onDragOver={dragLeave}
                                                    />
                                                    <div className="imageOverlayDiv">
                                                        <span>
                                                            <i className="fa fa-trash-o trashIcon" onClick={() => deleteImage(items, i)}>  </i>
                                                        </span>
                                                    </div>
                                                </div>
                                            )
                                            :
                                            ''
                                        }
                                    </Grid.Row>
                                </Grid>

                                <div className="buttons">
                                    <Button
                                        type="submit"
                                        // disabled={submitting || pristine}
                                        content={buildingId ? 'Update' : 'Create'}
                                    />
                                </div>
                            </form>
                        )}
                    />
                </PageForm>
            </RowContainerFull>
        </Fragment>
    );
}

export default connect(mapState, mapDispatch)(BuildingCreateForm);


/*
All validation
*/