import React from 'react';
import { connect } from 'react-redux';

import { fetchRoomsStart } from '../../redux/rooms/room.actions';
import { selectRoomList } from '../../redux/rooms/room.selectors';
import { createStructuredSelector } from 'reselect';

const mapState = createStructuredSelector({
    rooms: selectRoomList
})

class RoomListings extends React.Component {

    componentDidMount() {
        const { fetchRoomsStart } = this.props;
        fetchRoomsStart();
    }

    render() {
        const { rooms } = this.props;
        return (
            <div>
                Room listings
            {rooms.rooms && rooms.rooms.map(room => (
            <div key={room.id}>{room.id}</div>
            ))
            }

            </div>
        )
    }
}

const mapDispatch = dispatch => ({
    fetchRoomsStart: () => dispatch(fetchRoomsStart())
});

export default connect(mapState, mapDispatch)(RoomListings);



/*
Get 

import React from 'react';

import { firestore } from '../../firebase/firebase';

class RoomListings extends React.Component {
    unsubscribeFromRoomsSnaphot = null;

    componentDidMount() {
        const roomsRef = firestore.collection('rooms');
        roomsRef.onSnapshot(async snapshot => {
            console.log(snapshot);
        })
    }

    render() {
        return (
            <div>
                Room listings
            </div>
        )
    }
}

export default RoomListings;
*/