import styled from 'styled-components';

export const HomepageTableContainer = styled.div`
    background: ${props => props.theme.white};
    padding: 24px;
    border-radius: 5px;
    box-shadow: 0 0 2rem 0 rgba(136,152,170,.15);

    h2 {
        font-size: 24px;
        color: #000;
        font-weight: 900;
        letter-spacing: 1px;
    }

    table {
        .one {
            font-size: 24px;
            text-align: center;
        }
    }
`