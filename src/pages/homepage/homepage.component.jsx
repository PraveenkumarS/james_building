import React, { Fragment } from 'react';
import Header from '../../components/header/header.component';
import { Grid } from 'semantic-ui-react';
import { RowContainer } from '../../index.styles';
import SingleCard from '../../components/stats/single-card.component';
import Graph from '../../components/stats/graph.component';
import FeedList from '../../components/feed/feed.component';
import PieChartGraph from '../../components/stats/pie-chart.component';
import HomepageMessages from './homepage-messages.component';

const Homepage = () => {
    return (
        <Fragment>
            <Header />
            <RowContainer>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <SingleCard stats="1,328,536" collection="Users" uplift="1.24" icon="user outline" color="red" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="47,016" collection="Members" uplift="1.31" icon="home" color="orange" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="52,798" collection="Rooms" uplift="1.38" icon="bed" color="yellow" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="567,230" collection="Viewings" uplift="2.11" icon="eye" color="blue" />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <SingleCard stats="63,913" collection="Applications" uplift="1.18" icon="file alternate outline" color="green" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="93.07%" collection="Occupancy" uplift="2.84" icon="users" color="purple" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="1,043" collection="Partners" uplift="1.53" icon="handshake outline" color="pink" />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <SingleCard stats="12,074" collection="Events" uplift="2.49" icon="cocktail" color="orange" />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            <Graph />
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <FeedList />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={8}>  
                            <HomepageMessages />
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <PieChartGraph />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </RowContainer>
        </Fragment>
    )
}

export default Homepage;

/*
Move fetching of all stats into APP.js then you can just access from each component
*/