import React, { Component } from 'react';
import { Table, Image, Button } from 'semantic-ui-react';
import { HomepageTableContainer } from './homepage.styles';
import { Link } from 'react-router-dom';

class HomepageMessages extends Component {
    render() {
        return (
            <HomepageTableContainer>
                <h2>Your messages:</h2>
                <Table celled striped>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell width={1}><Image avatar src='https://firebasestorage.googleapis.com/v0/b/neighbourhood-9eab2.appspot.com/o/dummy-images%2Fpic1.jpg?alt=media&token=e52840f9-cdb8-44c7-ab6a-4c788b5f204a' /></Table.Cell>
                            <Table.Cell width={3}>Sally</Table.Cell>
                            <Table.Cell width={9}>Hello, can you help me with the following...</Table.Cell>
                            <Table.Cell width={3}><Button as={Link} to={`/`}>View</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell width={1}><Image avatar src='https://firebasestorage.googleapis.com/v0/b/neighbourhood-9eab2.appspot.com/o/dummy-images%2Fpic2.jpg?alt=media&token=ea4ed637-0b30-4c0b-abc1-eb98aebe1145' /></Table.Cell>
                            <Table.Cell width={3}>Tony</Table.Cell>
                            <Table.Cell width={9}>I have booked a viewing on the 21st of Ja...</Table.Cell>
                            <Table.Cell width={3}><Button as={Link} to={`/`}>View</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell width={1}><Image avatar src='https://firebasestorage.googleapis.com/v0/b/neighbourhood-9eab2.appspot.com/o/dummy-images%2Fpic3.jpeg?alt=media&token=1aedd532-d349-4d98-981d-ba636969fd8b' /></Table.Cell>
                            <Table.Cell width={3}>Catherine</Table.Cell>
                            <Table.Cell width={9}>I cannot seem to find my house key can yo...</Table.Cell>
                            <Table.Cell width={3}><Button as={Link} to={`/`}>View</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell width={1}><Image avatar src='https://firebasestorage.googleapis.com/v0/b/neighbourhood-9eab2.appspot.com/o/dummy-images%2Fprofile-fb-1510155084-7ff94f18ddd8.jpg?alt=media&token=e290bf6b-9530-48c5-9641-8b5c75dc4099' /></Table.Cell>
                            <Table.Cell width={3}>Alex</Table.Cell>
                            <Table.Cell width={9}>Would I be able to organise for the clean...</Table.Cell>
                            <Table.Cell width={3}><Button as={Link} to={`/`}>View</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell width={1}><Image avatar src='https://firebasestorage.googleapis.com/v0/b/neighbourhood-9eab2.appspot.com/o/dummy-images%2Fpic4.jpeg?alt=media&token=8decc850-4d07-41cd-8dab-7e7f22af30f7' /></Table.Cell>
                            <Table.Cell width={3}>Gary</Table.Cell>
                            <Table.Cell width={9}>Hello, can you help me with the following...</Table.Cell>
                            <Table.Cell width={3}><Button as={Link} to={`/`}>View</Button></Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </HomepageTableContainer>
        );
    }
}

export default HomepageMessages;