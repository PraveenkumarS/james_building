import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Homepage from '../pages/homepage/homepage.component';
import RoomListings from '../pages/rooms/rooms.component';
import NavBar from '../components/nav/nav-bar.component';
import VerticalNav from '../components/nav/nav-vertical.component';
import SignInForm from '../components/auth/sign-in-form.component';
import { MainContainer } from '../index.styles';

import { selectCurrentUser } from '../redux/users/user.selectors';
import { checkUserSession } from '../redux/users/user.actions';
import BuildingsDashboard from '../pages/buildings/dashboard/buildings-dashboard.component';
import BuildingDetail from '../pages/buildings/detail/building-detail.component';
import BuildingCreateForm from '../pages/buildings/create-form/building-form.component';
import NotFound from '../components/not-found/not-found.component';
import Calendar from '../pages/calendar/calendar-dashboard.component';

const mapState = createStructuredSelector ({
  currentUser: selectCurrentUser
})

const mapDispatch = dispatch => ({
  checkUserSession: () => dispatch(checkUserSession())
})

class App extends React.Component {
  unsubscribeFromAuth = null;

  componentDidMount() {
    const { checkUserSession } = this.props;
    checkUserSession();
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }

    render() {
      const { currentUser } = this.props;
      return (
        <Fragment>
          {currentUser ? (
            <Fragment>
            <NavBar currentUser={currentUser} />
            <VerticalNav />
            <MainContainer className="main">
              <Switch>
                <Route exact path='/' component={Homepage} />
                <Route exact path='/rooms' component={RoomListings} />
                <Route exact path='/buildings' component={BuildingsDashboard} />
                <Route path='/buildings/:id' component={BuildingDetail} />
                <Route exact path={['/building-create', '/building-edit/:id']} 
                    component={BuildingCreateForm}  />
                <Route exact path='/calendar' component={Calendar} />
                <Route component={NotFound} />
              </Switch>
            </MainContainer>
            </Fragment>
          ) : (
            <SignInForm />
          )}            
        </Fragment>
      )
    }
}

export default connect(mapState, mapDispatch)(App);


/*
      <Fragment>
          {currentUser ? (
            <Fragment>
              <NavBar currentUser={currentUser} />
              <VerticalNav />
              <div className="main">
                <Switch>
                  <Route exact path='/' component={Homepage} />
                  <Route exact path='/rooms' component={RoomListings} />
                </Switch>
              </div>
            </Fragment>
          ) : (
            <SignInForm />
          )}
        </Fragment>
*/