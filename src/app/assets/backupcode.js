  /* building search reducer 
  case BuildingActionTypes.BUILDING_SEARCH_START:
    return {
      ...state,
      isFetching: true
    };
  case BuildingActionTypes.BUILDING_SEARCH_FAILURE:
    return {
      ...state,
      isFetching: false,
      errorMessage: action.payload,
      buildings: []
    };
    
    
    
    
    
    building search component
    import React from 'react';
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import {buildingSearchStart} from '../../../redux/buildings/building.actions';

const mapDispatch = dispatch => ({
    buildingSearchStart: (searchQueryParams) => dispatch(buildingSearchStart(searchQueryParams))
})

const BuildingsSearch = ({buildingSearchStart}) => {

    const handleSubmit = async values => {
        console.log(values);
        const fieldSelected = values.fieldSelected;
        const searchQuery = values.searchQuery;
        buildingSearchStart({fieldSelected, searchQuery});
    };

    return (
        <div className='building-search-form'>
            <Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                <form onSubmit={handleSubmit}>
                    <Field name="fieldSelected" component="select" defaultValue="area">
                        <option value="area">Area</option>
                        <option value="city">City</option>
                        <option value="country">Country</option>
                    </Field>
                    <Field
                        name="searchQuery"
                        component="input"
                        type="text"
                        placeholder="Search here..."
                    />
                    <div className="buttons">
                    <button type="submit" disabled={submitting || pristine}>
                        Search
                    </button>
                    </div>
                </form>
                )}
            />
        </div>
    );
}

export default connect(null, mapDispatch)(BuildingsSearch);


builing search actions? 
export const buildingSearchStart = searchQueryParams => ({
    type: BuildingActionTypes.BUILDING_SEARCH_START,
    payload: searchQueryParams
  });
  
  export const buildingsSearchFailure = errorMessage => ({
    type: BuildingActionTypes.BUILDING_SEARCH_FAILURE,
    payload: errorMessage
  });
    
    


  building sagas

  export function* buildingSearchAsync({ payload: { fieldSelected, searchQuery } }) {
  try {
    const buildingRef = firestore.collection('buildings');
    let searchRef = buildingRef.where(fieldSelected, '==', searchQuery);
    const snapshot = yield searchRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(fetchBuildingsSuccess(fetchedItems));
    } else {
      yield put(buildingsSearchFailure('No buildings found'));
    }
  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
} 

export function* buildingSearchStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_SEARCH_START,
    buildingSearchAsync
  );
}



/*
export function* buildingSearchAsync({ payload: { fieldSelected, searchQuery } }) {
  try {
    const buildingRef = firestore.collection('buildings');
    let searchRef = buildingRef.where(fieldSelected, '==', searchQuery);
    const snapshot = yield searchRef.get();

    let fetchedBuildings = [];
    for (let i = 0; i < snapshot.docs.length; i++) {
      let buildingQuery = {
        ...snapshot.docs[i].data(),
        id: snapshot.docs[i].id
      };
      fetchedBuildings.push(buildingQuery);
    }
    if (fetchedBuildings.length > 0) {
      yield put(fetchBuildingsSuccess(fetchedBuildings));
    } else {
      yield put(buildingsSearchFailure('No buildings found'));
    }
  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

 BUILDING_SEARCH_START: 'BUILDING_SEARCH_START',
    BUILDING_SEARCH_FAILURE: 'BUILDING_SEARCH_FAILURE',


building saga loop stuff
 // let searchRef;
    // values.city.map(theCity => (
    //   searchRef = buildingRef.where('city', '==', theCity)
    // ));
    // const snapshot = yield searchRef.get();
    // console.log(snapshot);

    // let array = [];
    // let x;
    // for (x of values.city) {
    //   searchRef = buildingRef.where('city', '==', x);
    //   const snapshot = yield searchRef.get();
    //   array.push(snapshot);
    // }

    // let array = [];
    // let x;
    // for (x of values.city) {
    //   array.push(
    //     yield buildingRef.where('city', '==', x).get()
    //   );      
    // }

    // let queryResults = [];
    // for (let i = 0; i < xyz.length; i++) {
    //   queryResults.push(
    //     yield buildingRef.where('city', '==', xyz[i]).get()
    //   )
    // }


    export function* filterBuildingsAsync({ payload: values }) {
  try {
    console.log(Object.keys(values).length)
    const buildingRef = firestore.collection('buildings');
    let searchRef;
    let x;
    let snapshot;
    let combinedItems = [];
    for (x of values.city) {
      searchRef = buildingRef.where('city', '==', x);
      snapshot = yield searchRef.get();
      for (let i = 0; i < snapshot.docs.length; i++) {
        let itemsQuery = {
          ...snapshot.docs[i].data(),
          id: snapshot.docs[i].id
        };
        combinedItems.push(itemsQuery);
      }
    }
    if (combinedItems.length > 0) {
      yield put(fetchBuildingsSuccess(combinedItems));
    } else {
      yield put(fetchBuildingsFailure('No buildings found'));
    }

  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

    let combinedItems = [];

    Object.keys(values).map((key, index) => {

    });

    const loopEachObject = values[key].forEach((innerKey, innerIndex) => {
      return buildingRef.where(key, '==', innerKey).get();
    });

    const loopSnaphot = yield all(loopEachObject);

    for (let i = 0; i < loopSnaphot.docs.length; i++) {
      let itemsQuery = {
         ...loopSnaphot.docs[i].data(),
        id: loopSnaphot.docs[i].id
      };
      combinedItems.push(itemsQuery);
    }

        // console.log(Object.keys(values).length);

    // let objectKey;
    // for (objectKey of Object.keys(values)) {
    //   console.log(objectKey);
    // }

    // const buildingRef = firestore.collection('buildings');
    // let combinedItems = [];
    // Object.keys(values).forEach((key, index) => {
    //   console.log(key, values[key], values[key].length);
    //   let searchRef;
    //   let snapshot;
    //   values[key].forEach((innerKey, innerIndex) => {
    //     searchRef = buildingRef.where(key, '==', innerKey);
    //     snapshot = searchRef.get();
    //     console.log(key, innerKey);
    //   });
    // });

    // const buildingRef = firestore.collection('buildings');
    // let searchRef;
    // let x;
    // let snapshot;
    // let combinedItems = [];
    // for (x of values.city) {
    //   searchRef = buildingRef.where('city', '==', x);
    //   snapshot = yield searchRef.get();
    //   for (let i = 0; i < snapshot.docs.length; i++) {
    //     let itemsQuery = {
    //       ...snapshot.docs[i].data(),
    //       id: snapshot.docs[i].id
    //     };
    //     combinedItems.push(itemsQuery);
    //   }
    // }


filter buildings on state
export const selectBuilding = buildingUrlParam => createSelector(
    [selectBuildingList], 
    buildings => buildings.filter(building => building.id === buildingUrlParam)[0] || {}
);

Does this work if no buildings in state? 
const mapState = (state, ownProps) => {
  return {
      building: selectBuilding(ownProps.match.params.id)(state)
  }
};




import BuildingActionTypes from './building.types';

const INITIAL_STATE = {
  buildings: null,
  isFetching: false,
  errorMessage: undefined,
  building: {
    building: null,
    buildingPhotos: null,
    buildingDocuments: null,
    buildingRooms: null,
    isFetchingBuilding: false,
    isFetchingBuildingPhotos: false,
    isFetchingBuildingRooms: false,
    isFetchingBuildingDocs: false
  }
};

const buildingReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BuildingActionTypes.FETCH_BUILDINGS_START:
    case BuildingActionTypes.FILTER_BUILDINGS_START:
      return {
        ...state,
        isFetching: true
      };

    case BuildingActionTypes.SINGLE_BUILDING_START:
      return {
        ...state,
        building: {
          ...state.building,
          isFetchingBuilding: true
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_START:
      return {
        ...state,
        building: {
          ...state.building,
          isFetchingBuildingPhotos: true
        }
      };

    case BuildingActionTypes.BUILDING_ROOMS_START:
      return {
        ...state,
        building: {
          ...state.building,
          isFetchingBuildingRooms: true
        }
      };

    case BuildingActionTypes.BUILDING_DOCUMENTS_START:
      return {
        ...state,
        building: {
          ...state.building,
          isFetchingBuildingDocs: true
        }
      };



    case BuildingActionTypes.FETCH_BUILDINGS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        buildings: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          isFetchingBuilding: false,
          building: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          isFetchingBuildingPhotos: false,
          buildingPhotos: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_ROOMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          isFetchingBuildingRooms: false,
          buildingRooms: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_DOCUMENTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          isFetchingBuildingDocs: false,
          buildingDocuments: action.payload
        }
      };
      
   

   

    case BuildingActionTypes.FETCH_BUILDINGS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
        building: {
          ...state.building,
          isFetchingBuilding: false
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_FAILURE:
    return {
      ...state,
      isFetching: false,
      errorMessage: action.payload,
      building: {
        ...state.building,
        isFetchingBuildingPhotos: false
      }
    };

    case BuildingActionTypes.BUILDING_ROOMS_FAILURE:
    return {
      ...state,
      isFetching: false,
      errorMessage: action.payload,
      building: {
        ...state.building,
        isFetchingBuildingRooms: false
      }
    };

    case BuildingActionTypes.BUILDING_DOCUMENTS_FAILURE:
    return {
      ...state,
      isFetching: false,
      errorMessage: action.payload,
      building: {
        ...state.building,
        isFetchingBuildingDocs: false
      }
    };

    default:
      return state;
  }
};

export default buildingReducer;



import {createSelector} from 'reselect';

const selectBuildings = state => state.buildings;

export const selectBuildingList = createSelector(
    [selectBuildings],
    buildings => (buildings.buildings ? buildings.buildings : [])
    
);

export const selectBuildingOffState = createSelector(
    [selectBuildings],
    buildings => (buildings.building ? buildings.building : [])
);




export const selectBuildingOffBuilding = createSelector(
    [selectBuildingOffState],
    building => (building.building ? building.building : [])
);

export const selectBuilding = createSelector(
    [selectBuildingOffBuilding], 
    building => (building ? building : [])
);




export const selectDocumentsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => (building.buildingDocuments ? building.buildingDocuments : [])
);

export const buildingDocumentList = createSelector(
    [selectDocumentsOffBuilding],
    buildingDocuments => (buildingDocuments ? buildingDocuments : [])
);



export const selectPhotosOffBuilding = createSelector(
    [selectBuildingOffState],
    building => (building.buildingPhotos ? building.buildingPhotos : [])
);

export const buildingPhotoList = createSelector(
    [selectPhotosOffBuilding],
    buildingPhotos => (buildingPhotos ? buildingPhotos : [])
);



export const selectRoomsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => (building.buildingRooms ? building.buildingRooms : [])
);

export const buildingRoomList = createSelector(
    [selectRoomsOffBuilding],
    buildingRooms => (buildingRooms ? buildingRooms : [])
);


// import { createStructuredSelector } from 'reselect';
// const mapState = createStructuredSelector({
//     initialValues: selectBuilding
// });
// import { selectBuilding, selectFetchingBuilding } from '../../../redux/buildings/building.selectors';




exports.onWriteBuildingActivity = functions.firestore
  .document('buildings/{buildingId}')
  .onCreate((building, context) => {
    let onWriteBuilding;
    if(!building.before.data && building.after.data){ 
        console.log('Created');
        newBuilding = building.data();
        onWriteBuilding = {
            type: 'buildingCreated',
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            buildingId: building.id,
            creatorName: newBuilding.creatorName,
            creatorId: newBuilding.creatorId,
            creatorPhoto: newBuilding.creatorPhoto
        }
    }
    else if(building.before.data && building.after.data){ 
        console.log('Update');
        let updatedBuilding = building.after.data();
        onWriteBuilding = {
            type: 'buildingUpdated',
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            buildingId: context.params.buildingId,
            creatorName: updatedBuilding.lastUpdatedByName,
            creatorId: updatedBuilding.lastUpdatedById,
            creatorPhoto: updatedBuilding.lastUpdatedByPhoto
        }
    }
    else if(building.after.data === undefined){ 
        console.log('Deleted');
        deletedBuilding = building.data();
        onWriteBuilding = {
            type: 'buildingDeleted',
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            buildingId: building.id,
            creatorName: deletedBuilding.creatorName,
            creatorId: deletedBuilding.creatorId,
            creatorPhoto: deletedBuilding.creatorPhoto
        }
    } else {
        return console.log('Building activity did not meet any of the conditions ');
    }
    return admin
      .firestore()
      .collection('activity')
      .add(onWriteBuilding)
      .then(docRef => {
        return console.log('Activity created with ID: ', docRef.id);
      })
      .catch(err => {
        return console.log('Error adding activity: ', err);
      });
  });



  import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingsOffState, selectFetchingOffBuildingsState } from '../../../redux/buildings/building.selectors';
import { createStructuredSelector } from 'reselect';

import Header from '../../../components/header/header.component';
import { RowContainerFull } from '../../../index.styles';
import PageDashboard from '../../../components/page-dashboard/page-dashboard.component';
import {PageHeader} from '../../../components/page-dashboard/page-dashboard.styles';
import { Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import LoadingComponent from '../../../components/with-spinner/loading.component';



const mapState = createStructuredSelector({
    buildings: selectBuildingsOffState,
    isLoading: selectFetchingOffBuildingsState
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values))
});

const filters = [
    { name: 'city', innerArray: ['Manchester', 'London', 'Bristol'] },
    { name: 'area', innerArray: ['Shoreditch', 'Castlefield', 'Northern Quarter'] },
    { name: 'developer', innerArray: ['GSK Property', 'Yoo Capital', 'Chris Co'] }
];

const columns = ['name', 'area', 'city', 'country', 'units', 'rooms', 'developer'];

const BuildingsDashboard = ({buildings, applyBuildingFilters, fetchBuildingsStart, isLoading}) => {
    useEffect(() => {
        fetchBuildingsStart();
    }, [fetchBuildingsStart]);

    const applyFilters = (values) => {
        applyBuildingFilters(values);
    }
    if (isLoading) return <LoadingComponent />;
    return (
        <Fragment>
            <Header />
            <RowContainerFull>
                <PageHeader>
                    <div className="copy dashboard">
                        <Icon name="building outline" />
                        <div className="header">
                            <h1>Buildings</h1>
                        </div>
                    </div>
                    <div className="stats">
                        <div className="first stat">
                            <span className="title">Total</span>
                            <span className="number">21</span>
                        </div>
                        <div className="second stat">
                            <span className="title">Available Units</span>
                            <span className="number">64</span>
                        </div>
                    </div>
                    <div className="buttons">
                        <Button as={Link} to='/building-create' className="create">
                            Create Building
                        </Button>
                    </div>
                </PageHeader>
                <PageDashboard initialFetched={buildings} filters={filters} columns={columns} applyFilters={applyFilters} />
            </RowContainerFull>
        </Fragment>
    );
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);



/*
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingList } from '../../../redux/buildings/building.selectors';
import { createStructuredSelector } from 'reselect';

import Header from '../../../components/header/header.component';
import { RowContainer } from '../../../index.styles';
import { Button } from 'semantic-ui-react';
import PageDashboard from '../../../components/page-dashboard/page-dashboard.component';



const mapState = createStructuredSelector({
    buildings: selectBuildingList
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values))
});

const filters = [
    { name: 'city', innerArray: ['Manchester', 'London', 'Bristol']},
    { name: 'area', innerArray: ['Shoreditch', 'Castlefield', 'Northern Quarter']}
];

class BuildingsDashboard extends Component {

    componentDidMount() {
        const { fetchBuildingsStart } = this.props;
        fetchBuildingsStart();
    }

    applyFilters = (values) => {
        const { applyBuildingFilters } = this.props;
        applyBuildingFilters(values);
        console.log('apply filters function');
    }

    render() {
        const { buildings } = this.props;
        return (
            <Fragment>
                <Header />
                <RowContainer>
                    <div className="add-button">
                        <Button>Add Property</Button>
                    </div>
                    <PageDashboard initialFetched={buildings} filters={filters} applyFilters={this.applyFilters} />
                </RowContainer>
            </Fragment>
        );
    }
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);




import BuildingActionTypes from './building.types';

const INITIAL_STATE = {
  buildings: null,
  isFetching: false,
  errorMessage: undefined,
  building: {
    building: null,
    buildingPhotos: null,
    buildingDocuments: null,
    buildingRooms: null,
    isFetchingBuilding: false
  }
};

const buildingReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BuildingActionTypes.FETCH_BUILDINGS_START:
    case BuildingActionTypes.FILTER_BUILDINGS_START:
    case BuildingActionTypes.BUILDING_PHOTOS_START:
    case BuildingActionTypes.BUILDING_ROOMS_START:
    case BuildingActionTypes.BUILDING_DOCUMENTS_START:
      return {
        ...state,
        isFetching: true
      };

    case BuildingActionTypes.SINGLE_BUILDING_START:
      return {
        ...state,
        isFetching: true,
        building: {
          ...state.building,
          isFetchingBuilding: true,
          createdBuildingId: null
        }
      };

    case BuildingActionTypes.CREATE_BUILDING_START:
      return {
        ...state,
        building: {
          ...state.building,
          createdBuildingId: null
        }
      };



    case BuildingActionTypes.FETCH_BUILDINGS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        buildings: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          isFetchingBuilding: false,
          building: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          buildingPhotos: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_ROOMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          buildingRooms: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_DOCUMENTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        building: {
          ...state.building,
          buildingDocuments: action.payload
        }
      };

   

    case BuildingActionTypes.FETCH_BUILDINGS_FAILURE:
    case BuildingActionTypes.BUILDING_ROOMS_FAILURE:
    case BuildingActionTypes.BUILDING_DOCUMENTS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
        building: {
          ...state.building,
          isFetchingBuilding: false
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
        building: {
          ...state.building,
          buildingPhotos: null
        }
      };

    default:
      return state;
  }
};

export default buildingReducer;


import {createSelector} from 'reselect';

const selectBuildings = state => state.buildings;

export const selectBuildingsOffState = createSelector(
    [selectBuildings],
    buildings => (buildings.buildings ? buildings.buildings : [])  
);

export const selectBuildingOffState = createSelector(
    [selectBuildings],
    buildings => buildings.building
);



export const selectFetchingOffBuildingsState = createSelector(
    [selectBuildings],
    buildings => buildings.isFetching
);

export const selectFetchingBuilding = createSelector(
    [selectBuildingOffState],
    building => building.isFetchingBuilding
);


export const selectBuildingOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.building
);

export const selectBuilding = createSelector(
    [selectBuildingOffBuilding], 
    building => (building ? building : [])
);





export const selectDocumentsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingDocuments
);

export const buildingDocumentList = createSelector(
    [selectDocumentsOffBuilding],
    buildingDocuments => buildingDocuments
);



export const selectPhotosOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingPhotos
);

export const buildingPhotoList = createSelector(
    [selectPhotosOffBuilding],
    buildingPhotos => buildingPhotos
);



export const selectRoomsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingRooms
);

export const buildingRoomList = createSelector(
    [selectRoomsOffBuilding],
    buildingRooms => buildingRooms
);


import BuildingActionTypes from './building.types';

export const fetchBuildingsStart = () => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_START
});

export const fetchBuildingsSuccess = buildingsMap => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_SUCCESS,
  payload: buildingsMap
});

export const fetchBuildingsFailure = errorMessage => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_FAILURE,
  payload: errorMessage
});

export const applyBuildingFilters = values => ({
  type: BuildingActionTypes.FILTER_BUILDINGS_START,
  payload: values
});

export const singleBuildingStart = buildingId => ({
  type: BuildingActionTypes.SINGLE_BUILDING_START,
  payload: buildingId
});

export const singleBuildingSuccess = buildingData => ({
  type: BuildingActionTypes.SINGLE_BUILDING_SUCCESS,
  payload: buildingData
});

export const singleBuildingFailure = errorMessage => ({
  type: BuildingActionTypes.SINGLE_BUILDING_FAILURE,
  payload: errorMessage
});

export const fetchBuildingPhotos = buildingId => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_START,
  payload: buildingId
});

export const buildingPhotosSuccess = photosMap => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_SUCCESS,
  payload: photosMap
});

export const buildingPhotosFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_FAILURE,
  payload: errorMessage
});

export const fetchBuildingDocuments = buildingId => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_START,
  payload: buildingId
});

export const buildingDocumentsSuccess = documentsMap => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_SUCCESS,
  payload: documentsMap
});

export const buildingDocumentsFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_FAILURE,
  payload: errorMessage
});

export const buildingRoomsStart = buildingId => ({
  type: BuildingActionTypes.BUILDING_ROOMS_START,
  payload: buildingId
});

export const buildingRoomsSuccess = roomsMap => ({
  type: BuildingActionTypes.BUILDING_ROOMS_SUCCESS,
  payload: roomsMap
});

export const buildingRoomsFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_ROOMS_FAILURE,
  payload: errorMessage
});

export const createBuilding = values => ({
  type: BuildingActionTypes.CREATE_BUILDING_START,
  payload: values
});

export const deleteBuilding = buildingId => ({
  type: BuildingActionTypes.DELETE_BUILDING_START,
  payload: buildingId
});


import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { firestore } from '../../firebase/firebase';
import { 
  fetchBuildingsSuccess, 
  fetchBuildingsFailure, 
  buildingRoomsSuccess, 
  buildingRoomsFailure, 
  buildingPhotosSuccess, 
  buildingPhotosFailure, 
  buildingDocumentsSuccess, 
  buildingDocumentsFailure,
  singleBuildingSuccess,
  singleBuildingFailure
} from './building.actions';
import BuildingActionTypes from './building.types';
import { loopHelper } from '../../components/helpers/helpers';
import { toastr } from 'react-redux-toastr';
import history from '../../components/helpers/history';
import {selectCurrentUser} from '../users/user.selectors';


export function* fetchBuildingsAsync() {
  try {
    const buildingRef = firestore.collection('buildings');
    const snapshot = yield buildingRef.get();
    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    yield put(fetchBuildingsSuccess(fetchedItems));
  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

export function* filterBuildingsAsync({ payload: values }) {
  try {
    const buildingRef = firestore.collection('buildings');
    let combinedItems = [];
    yield* Object.keys(values).map(function* (key) {
      yield* values[key].map(function* (innerKey) {
        let snapshot = yield buildingRef.where(key, '==', innerKey).get();
          for (let i = 0; i < snapshot.docs.length; i++) {
            let itemsQuery = {
              ...snapshot.docs[i].data(),
              id: snapshot.docs[i].id
            };
            combinedItems.push(itemsQuery);
          }
      });
    });
     
    let filtered = combinedItems.reduce((filtered, item) => {
      if( !filtered.some(filteredItem => JSON.stringify(filteredItem) === JSON.stringify(item)) )
        filtered.push(item)
      return filtered
    }, [])
    if (filtered.length > 0) {
      yield put(fetchBuildingsSuccess(filtered));
    } else {
      yield put(fetchBuildingsFailure('No buildings found'));
    }

  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

export function* singleBuildingAsync({ payload: buildingID }) {
  try {
    let singleBuildingRef = firestore.collection('buildings').doc(buildingID);
    let snapshot = yield singleBuildingRef.get();
    let snapshotData = snapshot.data();
    let docData = {
      ...snapshotData,
      id: snapshot.id
    };
    yield put(singleBuildingSuccess(docData));
  } catch (error) {
      yield put(singleBuildingFailure(error.message));
  }
}

export function* buildingPhotosAsync({ payload: buildingId }) {
  try {
    const photosRef = firestore.collection(`buildings/${buildingId}/photos`);
    const snapshot = yield photosRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(buildingPhotosSuccess(fetchedItems));
    } else {
      yield put(buildingPhotosFailure('No photos found'));
    }
  } catch (error) {
    yield put(buildingPhotosFailure(error.message));
  }
}

export function* buildingDocumentsAsync({ payload: buildingId }) {
  try {
    const documentsRef = firestore.collection(`buildings/${buildingId}/documents`);
    const snapshot = yield documentsRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(buildingDocumentsSuccess(fetchedItems));
    } else {
      yield put(buildingDocumentsFailure('No Docs found'));
    }
  } catch (error) {
    yield put(buildingDocumentsFailure(error.message));
  }
}

export function* buildingRoomsAsync({ payload: buildingId }) {
  try {
    const roomsRef = firestore.collection('rooms');
    let searchRef = roomsRef.where('buildingId', '==', buildingId);
    const snapshot = yield searchRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(buildingRoomsSuccess(fetchedItems));
    } else {
      yield put(buildingRoomsFailure('No rooms found'));
    }
  } catch (error) {
    yield put(buildingRoomsFailure(error.message));
  }
}

export function* createBuildingAsync({ payload: values }) {
  try {
    const createdAt = new Date();
    const buildingsCollection = firestore.collection('buildings');
    let authDoc = yield select(selectCurrentUser);

    if (values.id) {
      const buildingRef = buildingsCollection.doc(values.id);
      const snapshot = yield buildingRef.get();

      if (!snapshot.exists) {
        toastr.error('Error!', 'Building does not exist');
      } else {
        try {
          yield buildingRef.update({
            ...values,
            lastUpdatedById: authDoc.id,
            lastUpdatedByName: authDoc.displayName,
            lastUpdatedByPhoto: authDoc.photoURL 
          });
          toastr.success('Success!', 'Building has been updated');
          history.push(`/buildings/${values.id}`)
        } catch (error) {
          console.log('error creating user', error.message);
        }
      }
    } else {
        try {
          let createdBuilding = yield buildingsCollection.add({
            ...values,
            createdAt,
            creatorId: authDoc.id,
            creatorName: authDoc.displayName,
            creatorPhoto: authDoc.photoURL
          });          
          toastr.success('Success!', 'Building has been created');
          history.push(`/buildings/${createdBuilding.id}`)
        } catch (error) {
          console.log('error creating user', error.message);
        }
    }
  } catch (error) {
    console.log(error);
    toastr.error('Error!', 'Please try again');
  }
}


export function* deleteBuildingAsync({ payload: buildingId }) {
  try {
    const buildingsCollection = firestore.collection('buildings');
    yield buildingsCollection.doc(buildingId).delete();
    toastr.success('Success!', 'Building has been deleted');
    history.push(`/buildings`)
  } catch (error) {
    console.log(error);
    toastr.error('Error!', 'Building not deleted');
  }
}







export function* fetchBuildingsStart() {
  yield takeLatest(
    BuildingActionTypes.FETCH_BUILDINGS_START,
    fetchBuildingsAsync
  );
}

export function* filterBuildingsStart() {
  yield takeLatest(
    BuildingActionTypes.FILTER_BUILDINGS_START,
    filterBuildingsAsync
  );
}

export function* singleBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.SINGLE_BUILDING_START,
    singleBuildingAsync
  );
}

export function* buildingPhotosStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_PHOTOS_START,
    buildingPhotosAsync
  );
}

export function* buildingDocumentsStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_DOCUMENTS_START,
    buildingDocumentsAsync
  );
}

export function* buildingRoomsStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_ROOMS_START,
    buildingRoomsAsync
  );
}

export function* createBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.CREATE_BUILDING_START,
    createBuildingAsync
  );
}

export function* deleteBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.DELETE_BUILDING_START,
    deleteBuildingAsync
  );
}

export function* buildingSagas() {
  yield all([
    call(fetchBuildingsStart),
    call(filterBuildingsStart),
    call(singleBuildingStart),
    call(buildingPhotosStart), 
    call(buildingRoomsStart), 
    call(buildingDocumentsStart),
    call(createBuildingStart),
    call(deleteBuildingStart)
  ]);
}


const BuildingActionTypes = {
    FETCH_BUILDINGS_START: 'FETCH_BUILDINGS_START',
    FETCH_BUILDINGS_SUCCESS: 'FETCH_BUILDINGS_SUCCESS',
    FETCH_BUILDINGS_FAILURE: 'FETCH_BUILDINGS_FAILURE',
    FILTER_BUILDINGS_START: 'FILTER_BUILDINGS_START',
    SINGLE_BUILDING_START: 'SINGLE_BUILDING_START',
    SINGLE_BUILDING_SUCCESS: 'SINGLE_BUILDING_SUCCESS',
    SINGLE_BUILDING_FAILURE: 'SINGLE_BUILDING_FAILURE',
    BUILDING_ROOMS_START: 'BUILDING_ROOMS_START',
    BUILDING_ROOMS_SUCCESS: 'BUILDING_ROOMS_SUCCESS',
    BUILDING_ROOMS_FAILURE: 'BUILDING_ROOMS_FAILURE',
    BUILDING_PHOTOS_START: 'BUILDING_PHOTOS_START',
    BUILDING_PHOTOS_SUCCESS: 'BUILDING_PHOTOS_SUCCESS',
    BUILDING_PHOTOS_FAILURE: 'BUILDING_PHOTOS_FAILURE',
    BUILDING_DOCUMENTS_START: 'BUILDING_DOCUMENTS_START',
    BUILDING_DOCUMENTS_SUCCESS: 'BUILDING_DOCUMENTS_SUCCESS',
    BUILDING_DOCUMENTS_FAILURE: 'BUILDING_DOCUMENTS_FAILURE',
    CREATE_BUILDING_START: 'CREATE_BUILDING_START',
    DELETE_BUILDING_START: 'DELETE_BUILDING_START'
  };
  
  export default BuildingActionTypes;



import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingsOffState } from '../../../redux/buildings/building.selectors';
import { createLoadingSelector } from '../../../redux/loading/loading.selectors';
import { createStructuredSelector } from 'reselect';
import Header from '../../../components/header/header.component';
import { RowContainerFull } from '../../../index.styles';
import PageDashboard from '../../../components/page-dashboard/page-dashboard.component';
import {PageHeader} from '../../../components/page-dashboard/page-dashboard.styles';
import { Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import LoadingComponent from '../../../components/with-spinner/loading.component';

const loadingSelector = createLoadingSelector(['FETCH_BUILDINGS']);

const mapState = createStructuredSelector({
    buildings: selectBuildingsOffState,
    isLoading: loadingSelector
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values))
});

const filters = [
    { name: 'city', innerArray: ['Manchester', 'London', 'Bristol'] },
    { name: 'area', innerArray: ['Shoreditch', 'Castlefield', 'Northern Quarter'] },
    { name: 'developer', innerArray: ['GSK Property', 'Yoo Capital', 'Chris Co'] }
];

const columns = ['name', 'area', 'city', 'country', 'units', 'rooms', 'developer'];

const BuildingsDashboard = ({buildings, applyBuildingFilters, fetchBuildingsStart, isLoading}) => {
    useEffect(() => {
        fetchBuildingsStart();
    }, []);

    const applyFilters = (values) => {
        applyBuildingFilters(values);
    }

    if (isLoading) return <LoadingComponent />

    return (
        <Fragment>
            <Header />
            <RowContainerFull>
                <PageHeader>
                    <div className="copy dashboard">
                        <Icon name="building outline" />
                        <div className="header">
                            <h1>Buildings</h1>
                        </div>
                    </div>
                    <div className="stats">
                        <div className="first stat">
                            <span className="title">Total</span>
                            <span className="number">21</span>
                        </div>
                        <div className="second stat">
                            <span className="title">Available Units</span>
                            <span className="number">64</span>
                        </div>
                    </div>
                    <div className="buttons">
                        <Button as={Link} to='/building-create' className="create">
                            Create Building
                        </Button>
                    </div>
                </PageHeader>
                <PageDashboard initialFetched={buildings} filters={filters} columns={columns} applyFilters={applyFilters} />
            </RowContainerFull>
        </Fragment>
    );
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);



/*
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBuildingsStart, applyBuildingFilters } from '../../../redux/buildings/building.actions';
import { selectBuildingList } from '../../../redux/buildings/building.selectors';
import { createStructuredSelector } from 'reselect';

import Header from '../../../components/header/header.component';
import { RowContainer } from '../../../index.styles';
import { Button } from 'semantic-ui-react';
import PageDashboard from '../../../components/page-dashboard/page-dashboard.component';



const mapState = createStructuredSelector({
    buildings: selectBuildingList
});

const mapDispatch = dispatch => ({
    fetchBuildingsStart: () => dispatch(fetchBuildingsStart()),
    applyBuildingFilters: (values) => dispatch(applyBuildingFilters(values))
});

const filters = [
    { name: 'city', innerArray: ['Manchester', 'London', 'Bristol']},
    { name: 'area', innerArray: ['Shoreditch', 'Castlefield', 'Northern Quarter']}
];

class BuildingsDashboard extends Component {

    componentDidMount() {
        const { fetchBuildingsStart } = this.props;
        fetchBuildingsStart();
    }

    applyFilters = (values) => {
        const { applyBuildingFilters } = this.props;
        applyBuildingFilters(values);
        console.log('apply filters function');
    }

    render() {
        const { buildings } = this.props;
        return (
            <Fragment>
                <Header />
                <RowContainer>
                    <div className="add-button">
                        <Button>Add Property</Button>
                    </div>
                    <PageDashboard initialFetched={buildings} filters={filters} applyFilters={this.applyFilters} />
                </RowContainer>
            </Fragment>
        );
    }
}

export default connect(mapState, mapDispatch)(BuildingsDashboard);
*/

/*
import React from 'react';
import { Form, Field } from 'react-final-form';
import { Button } from 'semantic-ui-react';

const PageDashFilters = ({filters, applyFilters}) => {

    const handleSubmit = async values => {
        applyFilters(values);
    };
    
    return (
        <div className="page-dash-filters-form">
            <div className="filters-section">
                <h2>Filters</h2>
            </div>
    
            <Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                <form onSubmit={handleSubmit}>                
                    {filters.map((filter, index) => (
                        <div className="filters-section" key={index}>
                            <h3>{filter.name}</h3>
                            {
                                filter.innerArray.map((value, subIndex) => (
                                    <div key={subIndex}>
                                        <label>{value}
                                        <Field 
                                            name={filter.name} 
                                            component="input" 
                                            type="checkbox"
                                            value={value}
                                        />
                                        <span className="checkmark"></span>
                                        </label>
                                    </div>
                                ))
                            }
                        </div>
                    ))}

                    <Button type="submit" disabled={submitting || pristine}>
                        Filter now
                    </Button>
                </form>
                )}
            />
        </div>
    );
}

export default PageDashFilters;*/

/*
{filters.map((filter, index) => {
                    return (
                        <div key={index}>
                            <label>{filter}</label>
                            <Field 
                                name={filter} 
                                component="input" 
                                type="checkbox"
                            />
                        </div>
                    )
                })}


{filters.map((filter, index) => (
                    <div key={index}>
                        <h3>{filter.name}</h3>
                        {
                            filter.innerArray.map((value, subIndex) => (
                                <div key={subIndex}>
                                    <label>{value}</label>
                                    <Field 
                                        name={filter.name} 
                                        component="input" 
                                        type="checkbox"
                                        value={value}
                                    />
                                </div>
                            ))
                        }
                    </div>
                ))}



                {countries && countryKeys.reverse().map((country, index) => (
                                                <option key={index} value={country}>{country}</option>
                                            ))};
*/


                            {/* {countryTitles.map((countryTitle, index) => (
                                <div className="filters-section" key={index}>
                                    
                                    {Object.keys(countries).map((country, subIndex) => (
                                        <div key={subIndex}>
                                            <label>{country}
                                                <Field 
                                                    name={country}
                                                    component="input" 
                                                    type="checkbox"
                                                    value={country}
                                                />
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                    ))}
                                </div>
                            ))} */}
                        {/* 
                        console.log(building_type, building_types[building_type])
                        building_types[building_type].map((value, subIndex) => (
                                    <div>{building_type[value]}</div>
                                ))
                        {Object.keys(building_types).map((building_type, index) => (
                            <div className="filters-section" key={index}>
                                <h3>{building_type}</h3>
                                {
                                    console.log(building_type),
                                    building_types[index].map((value, subIndex) => (
                                        <div key={subIndex}>
                                            <label>{value}
                                                <Field 
                                                    name={building_type} 
                                                    component="input" 
                                                    type="checkbox"
                                                    value={value}
                                                />
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                    ))
                                }
                            </div>
                        ))} */}


                        {/* <div className="filters-section">
                            <h3>{filterTitles[0]}</h3>
                            {Object.keys(filters).map((value, subIndex) => (
                                <div key={subIndex}>
                                    <label>{value}
                                        <Field 
                                            name={value} 
                                            component="input" 
                                            type="checkbox"
                                            value={value}
                                        />
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                            ))};
                        </div> */}
                    
/*
Checkbox

<div key={subIndex}>
                                    <label>{value}
                                        <Field 
                                            name={building_type}
                                            component="input" 
                                            type="checkbox"
                                            value={value}
                                        />
                                        <span className="checkmark"></span>
                                    </label>
                                </div>

label {
                font-size: 14px;
                display: block;
                position: relative;
                padding-left: 28px;
                margin-bottom: 12px;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                text-transform: capitalize;
    
                &:hover input[type=checkbox] ~ .checkmark {
                    background-color: #ccc;
                }
    
                input[type=checkbox] {
                    position: absolute;
                    opacity: 0;
                    cursor: pointer;
                }
    
                .checkmark {
                    position: absolute;
                    top: 0;
                    left: 0;
                    height: 20px;
                    width: 20px;
                    background-color: #eee;
    
                    &:after {
                        content: "";
                        position: absolute;
                        display: none;
                    }
                }
    
                input[type=checkbox]:checked ~ .checkmark {
                    background-color: #2196F3;
                }
    
                .checkmark:after {
                    content: "";
                    position: absolute;
                    display: none;
                }
    
                input[type=checkbox]:checked ~ .checkmark:after {
                    display: block;
                }
    
                .checkmark:after {
                    left: 7px;
                    top: 3px;
                    width: 6px;
                    height: 11px;
                    border: solid white;
                    border-width: 0 3px 3px 0;
                    -webkit-transform: rotate(45deg);
                    -ms-transform: rotate(45deg);
                    transform: rotate(45deg);
                }
            }


             {Object.keys(building_types).map((building_type, index) => (
                        <div className="filters-section" key={index}>
                            <h3>{building_type}</h3>
                            {Object.keys(building_types[building_type]).map((value, subIndex) => (
                                <div key={subIndex}>
                                    <Field 
                                        name="building_type"
                                        id={subIndex}
                                        component="input" 
                                        type="radio"
                                        value={value}
                                    />
                                    <label for={subIndex}>{value}</label>
                                </div>
                            ))}
                        </div>
                    ))}


                                        {/* {Object.keys(building_types).map((building_type, index) => (
                        <div className="filters-section" key={index}>
                            <h3>{building_type}</h3>
                            {Object.keys(building_types[building_type]).map((value, subIndex) => (
                                <div key={subIndex}>
                                    <Field 
                                        name="building_type"
                                        id={subIndex}
                                        component="input" 
                                        type="radio"
                                        value={value}
                                    />
                                    <label for={subIndex}>{value}</label>
                                </div>
                            ))}
                        </div>
                    ))}

                    export function* filterBuildingsAsync({ payload: values }) {
  try {
    console.log(values)
    const buildingRef = firestore.collection('buildings');
    let combinedItems = [];
    yield* Object.keys(values).map(function* (key) {
      console.log(key)
      yield* values[key].map(function* (innerKey) {
        let snapshot = yield buildingRef.where(key, '==', innerKey).get();
          for (let i = 0; i < snapshot.docs.length; i++) {
            let itemsQuery = {
              ...snapshot.docs[i].data(),
              id: snapshot.docs[i].id
            };
            combinedItems.push(itemsQuery);
          }
      });
    });
     
    let filtered = combinedItems.reduce((filtered, item) => {
      if( !filtered.some(filteredItem => JSON.stringify(filteredItem) === JSON.stringify(item)) )
        filtered.push(item)
      return filtered
    }, [])
    if (filtered.length > 0) {
      yield put(fetchBuildingsSuccess(filtered));
    } else {
      yield put(fetchBuildingsFailure('No buildings found'));
    }

  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

 const buildingRef = firestore.collection('buildings');
    let groupedQueries = [];
    let combinedItems = [];

    yield* Object.keys(values).map(function* (key) {
      let singleQuery = `(${key}, '==', ${values[key]})`
      groupedQueries.push(singleQuery);
      let snapshot = yield buildingRef.where.groupedQueries.get();
      console
      // yield* values[key].map(function* (innerKey) {
      //   let snapshot = yield buildingRef.where(key, '==', innerKey).get();
      //     for (let i = 0; i < snapshot.docs.length; i++) {
      //       let itemsQuery = {
      //         ...snapshot.docs[i].data(),
      //         id: snapshot.docs[i].id
      //       };
      //       combinedItems.push(itemsQuery);
      //     }
      // });
    });
     
    if (combinedItems.length > 0) {
      yield put(fetchBuildingsSuccess(combinedItems));
    } else {
      yield put(fetchBuildingsFailure('No buildings found'));
    }


    export function* singleBuildingAsync({ payload: buildingID }) {
  try {
    let singleBuildingRef = firestore.collection('buildings').doc(buildingID);
    let snapshot = yield singleBuildingRef.get();
    let snapshotData = snapshot.data();
    let docData = {
      ...snapshotData,
      id: snapshot.id
    };
    yield put(singleBuildingSuccess(docData));
  } catch (error) {
      yield put(singleBuildingFailure(error.message));
  }
}


export function* singleBuildingAsync({ payload: buildingID }) {
  try {
    let singleBuildingRef = firestore.collection('buildings').doc(buildingID);
    let doc = yield singleBuildingRef.get();
    if (doc.exists) {
      let snapshotData = doc.data();
      let docData = {
        ...snapshotData,
        id: doc.id
      };
      yield put(singleBuildingSuccess(docData));
    } else {
      yield put(singleBuildingFailure('Building not found'));
    }
  } catch (error) {
      yield put(singleBuildingFailure(error.message));
  }
}


const createSingleBuildingChannel = buildingID => {
  const listener = eventChannel(
    emit => {
      firestore.collection('buildings').doc(buildingID).onSnapshot(function(doc) {
        if (doc.exists) {
          emit(doc.data());
        } else {
          emit([])
        }
      });
      return () => firestore.collection('buildings').doc(buildingID).off(listener)
    }
  )
  return listener;
}

export function* singleBuildingAsync({ payload: buildingID }) {
  try {
    const updateDocChannel = yield createSingleBuildingChannel(buildingID);
    while (true) {
      const data = yield take(updateDocChannel);
      let docData = {
        ...data,
        id: buildingID
      };
      yield put(singleBuildingSuccess(docData));
    }
  } catch (error) {
    yield put(singleBuildingFailure(error.message));
  }
}



<Grid.Row>
                                    <Grid.Column>
                                        <Field name="country" component="select" control="select">
                                            <option value="none-selected">Select Country</option>
                                            {countries && countryKeys.reverse().map((country, index) => (
                                                <option key={index} value={country}>{country}</option>
                                            ))};
                                        </Field>
                                        <Field
                                            name="category"
                                            type="text"
                                            component={SelectInput}
                                            options={optionsArray}
                                            multiple={true}
                                            placeholder="Event Cat"
                                            />
                                        
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Field name="city" component="select">
                                            <option value="none-selected">Select City</option>
                                            {countryBoolean ? (
                                            Object.keys(countries[countrySelected]).map((city, index) => (
                                                <option key={index} value={city}>{city}</option>
                                            ))
                                            ) : (
                                                <option value={initialValues.city}>{initialValues.city}</option>
                                            )}
                                        </Field>
                                    </Grid.Column>
                                    <Grid.Column>
                                    <Field name="area" component="select">
                                        <option value="none-selected">Select Area</option>
                                        {cityBoolean ? (
                                            Object.keys(countries[countrySelected][citySelected]).map((area, index) => (
                                            <option key={index} value={area}>{area}</option>
                                        ))
                                        ) : (
                                            <option value={initialValues.area}>{initialValues.area}</option>
                                        )
                                        }
                                    </Field>
                                    </Grid.Column>
                                </Grid.Row>
*/