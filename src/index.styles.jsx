import styled from 'styled-components';

export const theme = {
    white: '#fff',
    red: '#f53240',
    green: '#02c8a7',
    cream: '#fcf5ed',
    grey: '#ebeded',
    medGrey: '#787878',
    darkGrey: '#2a2a2a',
    black: '#000',
    fontGrey: '#a3a6b1',
    lightBlue: '#11cdef',
    darkBlue: '#1171ef'
};

export const SectionContainer = styled.section`
  width: 100%;
`

export const RowContainer = styled.div`
  width: 100%;
  padding: 0 5%;
  margin-top: -342px;
`

export const RowContainerFull = styled.div`
  width: 100%;
  margin-top: -342px;
`

export const MainContainer = styled.div`
  width: 100%;
  padding-left: 250px;

  .ui.grid {
    > .row {
      padding-top: 20px;
      padding-bottom: 20px;
    }
  }
`
