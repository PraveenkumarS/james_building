/* eslint-disable default-case */
// import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';

var firebase = require('firebase');

const firebaseConfig = {
  apiKey: "AIzaSyDcTJL_FsNUcNK95Uq58I723vw9olZnBCc",
  authDomain: "neighbourhood-15ee9.firebaseapp.com",
  databaseURL: "https://neighbourhood-15ee9.firebaseio.com",
  projectId: "neighbourhood-15ee9",
  storageBucket: "neighbourhood-15ee9.appspot.com",
  messagingSenderId: "193946537294",
  appId: "1:193946537294:web:4fa44e664a96c40930621f",
  measurementId: "G-B7JNK6X73Z"
};

firebase.initializeApp(firebaseConfig);

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      unsubscribe();
      resolve(userAuth);
    }, reject);
  });
};

export function imageUpload (imageFile, timeStamp, resolve){
    var storageRef = firebase.storage().ref();
    var uploadTask = storageRef.child('images/'+timeStamp+"/"+imageFile.name).put(imageFile);
    uploadTask.on('state_changed', function(snapshot){
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log('Upload is running');
            break;
        }
      }, function(error) {
        }, function() {
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            resolve(downloadURL);
          });
      });
};

export function deleteUploadImages (image, resolve){
  var storageRef = firebase.storage().ref();
  var desertRef = storageRef.child(image);
  // Delete the file
  desertRef.delete().then(function() {
      resolve(true);
  }).catch(function(error) {
      resolve(false);
  });
}

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export default firebase;