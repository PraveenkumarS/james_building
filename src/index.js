import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store, persistor } from './redux/store';
import { ThemeProvider } from "styled-components";
import { PersistGate } from 'redux-persist/integration/react';
import ReduxToastr from 'react-redux-toastr';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import ScrollToTop from './components/helpers/scrollToTop';
import {theme} from './index.styles';
import './styles.scss';
import history from './components/helpers/history';

import App from './app/App';

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <PersistGate persistor={persistor}>
                <ThemeProvider theme={theme}>
                    <ScrollToTop>
                        <ReduxToastr
                            position='bottom-right'
                            transitionIn='fadeIn'
                            transitionOut='fadeOut'
                        />
                        <App />
                </ScrollToTop>
                </ThemeProvider>
            </PersistGate>
        </Router>
    </Provider>, 
    document.getElementById('root')
);




/*
Install modules
Remove unecessary files
Create folders
Create index styles
Create pages
Create routes
Add firebase config
Add redux
Add Sagas, ACtions, Reducers, Selectors, Types



Get stats working, update counters etc
*/
