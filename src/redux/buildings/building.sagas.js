import { takeLatest, call, put, all, select, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { firestore } from '../../firebase/firebase';
import {
  fetchBuildingsSuccess,
  fetchBuildingsFailure,
  filterBuildingsFailure,
  buildingRoomsSuccess,
  buildingRoomsFailure,
  buildingPhotosSuccess,
  buildingPhotosFailure,
  buildingDocumentsSuccess,
  buildingDocumentsFailure,
  singleBuildingSuccess,
  singleBuildingFailure
} from './building.actions';
import BuildingActionTypes from './building.types';
import { loopHelper } from '../../components/helpers/helpers';
import { toastr } from 'react-redux-toastr';
import history from '../../components/helpers/history';
import { selectCurrentUser } from '../users/user.selectors';


export function* fetchBuildingsAsync() {
  try {
    const buildingRef = firestore.collection('buildings').limit(25);
    const snapshot = yield buildingRef.get();
    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    yield put(fetchBuildingsSuccess(fetchedItems));
  } catch (error) {
    yield put(fetchBuildingsFailure(error.message));
  }
}

export function* filterBuildingsAsync({ payload: values }) {
  try {
    let buildingRef = firestore.collection('buildings');

    yield* Object.keys(values).map(function* (key) {
      buildingRef = yield buildingRef.where(key, '==', values[key]).limit(25);
    });
    let snapshot = yield buildingRef.get();
    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);

    if (fetchedItems.length > 0) {
      yield put(fetchBuildingsSuccess(fetchedItems));
    } else {
      yield put(filterBuildingsFailure('No buildings found'));
    }

  } catch (error) {
    yield put(filterBuildingsFailure(error.message));
  }
}

const createSingleBuildingChannel = buildingID => {
  const listener = eventChannel(
    emit => {
      firestore.collection('buildings').doc(buildingID).onSnapshot(function (doc) {
        emit(doc.data());
      });
      return () => firestore.collection('buildings').doc(buildingID).off(listener)
    }
  )
  return listener;
}

export function* singleBuildingAsync({ payload: buildingID }) {
  try {
    let singleBuildingRef = firestore.collection('buildings').doc(buildingID);
    let doc = yield singleBuildingRef.get();
    if (doc.exists) {
      const updateDocChannel = yield createSingleBuildingChannel(buildingID);
      while (true) {
        const data = yield take(updateDocChannel);
        let docData = {
          ...data,
          id: buildingID
        };
        yield put(singleBuildingSuccess(docData));
        yield put(buildingPhotosSuccess(data.photos));
      }
    } else {
      yield put(singleBuildingFailure('No building'));
    }
  } catch (error) {
    yield put(singleBuildingFailure(error.message));
  }
}

export function* buildingPhotosAsync({ payload: buildingId }) {
  // try {
  //   const photosRef = firestore.collection(`buildings/${buildingId}/photos`);
  //   const snapshot = yield photosRef.get();

  //   let fetchedItems = [];
  //   loopHelper(snapshot, fetchedItems);
  //   console.log("snapShot", fetchedItems);

  //   if (fetchedItems.length > 0) {
  //     yield put(buildingPhotosSuccess(fetchedItems));
  //   } else {
  //     yield put(buildingPhotosFailure('No photos found'));
  //   }
  // } catch (error) {
  //   yield put(buildingPhotosFailure(error.message));
  // }
}

export function* buildingDocumentsAsync({ payload: buildingId }) {
  try {
    const documentsRef = firestore.collection(`buildings/${buildingId}/documents`).limit(25);
    const snapshot = yield documentsRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(buildingDocumentsSuccess(fetchedItems));
    } else {
      yield put(buildingDocumentsFailure('No Docs found'));
    }
  } catch (error) {
    yield put(buildingDocumentsFailure(error.message));
  }
}

export function* buildingRoomsAsync({ payload: buildingId }) {
  try {
    const roomsRef = firestore.collection('rooms');
    let searchRef = roomsRef.where('buildingId', '==', buildingId);
    const snapshot = yield searchRef.get();

    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    if (fetchedItems.length > 0) {
      yield put(buildingRoomsSuccess(fetchedItems));
    } else {
      yield put(buildingRoomsFailure('No rooms found'));
    }
  } catch (error) {
    yield put(buildingRoomsFailure(error.message));
  }
}

export function* createBuildingAsync({ payload: values }) {
  try {
    const createdAt = new Date();
    const buildingsCollection = firestore.collection('buildings');
    let authDoc = yield select(selectCurrentUser);

    if (values.id) {
      const buildingRef = buildingsCollection.doc(values.id);
      const snapshot = yield buildingRef.get();

      if (!snapshot.exists) {
        toastr.error('Error!', 'Building does not exist');
      } else {
        try {
          yield buildingRef.update({
            ...values,
            lastUpdatedById: authDoc.id,
            lastUpdatedByName: authDoc.displayName,
            lastUpdatedByPhoto: authDoc.photoURL
          });
          toastr.success('Success!', 'Building has been updated');
          history.push(`/buildings/${values.id}`)
        } catch (error) {
          console.log('error creating user', error.message);
        }
      }
    } else {
      try {
        let createdBuilding = yield buildingsCollection.add({
          ...values,
          createdAt,
          creatorId: authDoc.id,
          creatorName: authDoc.displayName,
          creatorPhoto: authDoc.photoURL
        });

        toastr.success('Success!', 'Building has been created');

        history.push(`/buildings/${createdBuilding.id}`)
      } catch (error) {
        console.log('error creating user', error.message);
      }
    }
  } catch (error) {
    console.log(error);
    toastr.error('Error!', 'Please try again');
  }
}


export function* deleteBuildingAsync({ payload: buildingId }) {
  try {
    const buildingsCollection = firestore.collection('buildings');
    yield buildingsCollection.doc(buildingId).delete();
    toastr.success('Success!', 'Building has been deleted');
    history.push(`/buildings`)
  } catch (error) {
    console.log(error);
    toastr.error('Error!', 'Building not deleted');
  }
}


export function* fetchBuildingsStart() {
  yield takeLatest(
    BuildingActionTypes.FETCH_BUILDINGS_REQUEST,
    fetchBuildingsAsync
  );
}

export function* filterBuildingsStart() {
  yield takeLatest(
    BuildingActionTypes.FILTER_BUILDINGS_REQUEST,
    filterBuildingsAsync
  );
}

export function* singleBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.SINGLE_BUILDING_REQUEST,
    singleBuildingAsync
  );
}

export function* buildingPhotosStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_PHOTOS_REQUEST,
    buildingPhotosAsync
  );
}

export function* buildingDocumentsStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_DOCUMENTS_REQUEST,
    buildingDocumentsAsync
  );
}

export function* buildingRoomsStart() {
  yield takeLatest(
    BuildingActionTypes.BUILDING_ROOMS_REQUEST,
    buildingRoomsAsync
  );
}

export function* createBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.CREATE_BUILDING_REQUEST,
    createBuildingAsync
  );
}

export function* deleteBuildingStart() {
  yield takeLatest(
    BuildingActionTypes.DELETE_BUILDING_REQUEST,
    deleteBuildingAsync
  );
}

export function* buildingSagas() {
  yield all([
    call(fetchBuildingsStart),
    call(filterBuildingsStart),
    call(singleBuildingStart),
    call(buildingPhotosStart),
    call(buildingRoomsStart),
    call(buildingDocumentsStart),
    call(createBuildingStart),
    call(deleteBuildingStart)
  ]);
}

