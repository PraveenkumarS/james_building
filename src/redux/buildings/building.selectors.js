import {createSelector} from 'reselect';

const selectBuildings = state => state.buildings;

export const selectBuildingsOffState = createSelector(
    [selectBuildings],
    buildings => (buildings.buildings ? buildings.buildings : [])  
);

export const selectBuildingOffState = createSelector(
    [selectBuildings],
    buildings => buildings.building
);


export const selectBuildingOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.building
);

export const selectBuilding = createSelector(
    [selectBuildingOffBuilding], 
    building => (building ? building : [])
);

export const selectDocumentsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingDocuments
);

export const buildingDocumentList = createSelector(
    [selectDocumentsOffBuilding],
    buildingDocuments => buildingDocuments
);



export const selectPhotosOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingPhotos
);

export const buildingPhotoList = createSelector(
    [selectPhotosOffBuilding],
    buildingPhotos => buildingPhotos
);



export const selectRoomsOffBuilding = createSelector(
    [selectBuildingOffState],
    building => building.buildingRooms
);

export const buildingRoomList = createSelector(
    [selectRoomsOffBuilding],
    buildingRooms => buildingRooms
);



