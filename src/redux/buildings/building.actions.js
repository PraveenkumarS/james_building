import BuildingActionTypes from './building.types';

export const fetchBuildingsStart = () => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_REQUEST
});

export const fetchBuildingsSuccess = buildingsMap => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_SUCCESS,
  payload: buildingsMap
});

export const fetchBuildingsFailure = errorMessage => ({
  type: BuildingActionTypes.FETCH_BUILDINGS_FAILURE,
  payload: errorMessage
});

export const filterBuildingsFailure = errorMessage => ({
  type: BuildingActionTypes.FILTER_BUILDINGS_FAILURE,
  payload: errorMessage
});

export const applyBuildingFilters = values => ({
  type: BuildingActionTypes.FILTER_BUILDINGS_REQUEST,
  payload: values
});

export const singleBuildingStart = buildingId => ({
  type: BuildingActionTypes.SINGLE_BUILDING_REQUEST,
  payload: buildingId
});

export const singleBuildingSuccess = buildingData => ({
  type: BuildingActionTypes.SINGLE_BUILDING_SUCCESS,
  payload: buildingData
});

export const singleBuildingFailure = errorMessage => ({
  type: BuildingActionTypes.SINGLE_BUILDING_FAILURE,
  payload: errorMessage
});

export const fetchBuildingPhotos = buildingId => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_REQUEST,
  payload: buildingId
});

export const buildingPhotosSuccess = photosMap => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_SUCCESS,
  payload: photosMap
});

export const buildingPhotosFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_PHOTOS_FAILURE,
  payload: errorMessage
});

export const fetchBuildingDocuments = buildingId => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_REQUEST,
  payload: buildingId
});

export const buildingDocumentsSuccess = documentsMap => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_SUCCESS,
  payload: documentsMap
});

export const buildingDocumentsFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_DOCUMENTS_FAILURE,
  payload: errorMessage
});

export const buildingRoomsStart = buildingId => ({
  type: BuildingActionTypes.BUILDING_ROOMS_REQUEST,
  payload: buildingId
});

export const buildingRoomsSuccess = roomsMap => ({
  type: BuildingActionTypes.BUILDING_ROOMS_SUCCESS,
  payload: roomsMap
});

export const buildingRoomsFailure = errorMessage => ({
  type: BuildingActionTypes.BUILDING_ROOMS_FAILURE,
  payload: errorMessage
});

export const createBuilding = values => ({
  type: BuildingActionTypes.CREATE_BUILDING_REQUEST,
  payload: values
});

export const deleteBuilding = buildingId => ({
  type: BuildingActionTypes.DELETE_BUILDING_REQUEST,
  payload: buildingId
});