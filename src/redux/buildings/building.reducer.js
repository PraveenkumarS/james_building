import BuildingActionTypes from './building.types';

const INITIAL_STATE = {
  buildings: null,
  errorMessage: undefined,
  building: {
    building: null,
    buildingPhotos: null,
    buildingDocuments: null,
    buildingRooms: null
  }
};

const buildingReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BuildingActionTypes.FETCH_BUILDINGS_REQUEST:
    case BuildingActionTypes.FILTER_BUILDINGS_REQUEST:
    case BuildingActionTypes.BUILDING_PHOTOS_REQUEST:
    case BuildingActionTypes.BUILDING_ROOMS_REQUEST:
    case BuildingActionTypes.BUILDING_DOCUMENTS_REQUEST:
      return {
        ...state
      };

    case BuildingActionTypes.SINGLE_BUILDING_REQUEST:
      return {
        ...state,
        building: {
          ...state.building,
          createdBuildingId: null
        }
      };

    case BuildingActionTypes.CREATE_BUILDING_REQUEST:
      return {
        ...state,
        building: {
          ...state.building,
          createdBuildingId: null
        }
      };



    case BuildingActionTypes.FETCH_BUILDINGS_SUCCESS:
      return {
        ...state,
        buildings: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_SUCCESS:
      return {
        ...state,
        building: {
          ...state.building,
          building: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_SUCCESS:
      return {
        ...state,
        building: {
          ...state.building,
          buildingPhotos: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_ROOMS_SUCCESS:
      return {
        ...state,
        building: {
          ...state.building,
          buildingRooms: action.payload
        }
      };

    case BuildingActionTypes.BUILDING_DOCUMENTS_SUCCESS:
      return {
        ...state,
        building: {
          ...state.building,
          buildingDocuments: action.payload
        }
      };

   

    case BuildingActionTypes.FETCH_BUILDINGS_FAILURE:
    case BuildingActionTypes.BUILDING_ROOMS_FAILURE:
    case BuildingActionTypes.BUILDING_DOCUMENTS_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    case BuildingActionTypes.FILTER_BUILDINGS_FAILURE:
      return {
        ...state,
        buildings: [],
        errorMessage: action.payload
      };

    case BuildingActionTypes.SINGLE_BUILDING_FAILURE:
      return {
        ...state,
        errorMessage: action.payload,
        building: {
          ...state.building,
          building: []
        }
      };

    case BuildingActionTypes.BUILDING_PHOTOS_FAILURE:
      return {
        ...state,
        errorMessage: action.payload,
        building: {
          ...state.building,
          buildingPhotos: null
        }
      };

    default:
      return state;
  }
};

export default buildingReducer;