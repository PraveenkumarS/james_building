import CalendarActionTypes from './calendar.types';

export const fetchCalendarViewings = () => ({
  type: CalendarActionTypes.CALENDAR_VIEWINGS_REQUEST
});

export const calendarViewingsSuccess = viewingsMap => ({
  type: CalendarActionTypes.CALENDAR_VIEWINGS_SUCCESS,
  payload: viewingsMap
});

export const calendarViewingsFailure = errorMessage => ({
  type: CalendarActionTypes.CALENDAR_VIEWINGS_FAILURE,
  payload: errorMessage
});

