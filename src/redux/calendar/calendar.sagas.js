import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { firestore } from '../../firebase/firebase'; 
import { 
  calendarViewingsSuccess, 
  calendarViewingsFailure
} from './calendar.actions';
import CalendarActionTypes from './calendar.types';
import { loopHelper } from '../../components/helpers/helpers';
import {selectCurrentUser} from '../users/user.selectors';


export function* fetchCalendarViewingsAsync() {
  try {
    let authDoc = yield select(selectCurrentUser);
    const dateObj = new Date();
    const viewingsRef = firestore
    .collection('room_viewings')
    .where('team', '==', authDoc.team)
    .where('timeSlot', '>', dateObj);

    const snapshot = yield viewingsRef.get();
    let fetchedItems = [];
    loopHelper(snapshot, fetchedItems);
    yield put(calendarViewingsSuccess(fetchedItems));
  } catch (error) {
    yield put(calendarViewingsFailure(error.message));
  }
}

export function* fetchCalendarViewings() {
  yield takeLatest(
    CalendarActionTypes.CALENDAR_VIEWINGS_REQUEST,
    fetchCalendarViewingsAsync
  );
}

export function* calendarSagas() {
  yield all([
    call(fetchCalendarViewings)
  ]);
}

// Need to add team to each viewing document or can we do this via city??