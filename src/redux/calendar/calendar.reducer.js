import CalendarActionTypes from './calendar.types';

const INITIAL_STATE = {
  viewings: null,
  errorMessage: undefined
};

const calendarReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CalendarActionTypes.CALENDAR_VIEWINGS_REQUEST:
      return {
        ...state
      };

    case CalendarActionTypes.CALENDAR_VIEWINGS_SUCCESS:
      return {
        ...state,
        viewings: action.payload
      };

    case CalendarActionTypes.CALENDAR_VIEWINGS_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    default:
      return state;
  }
};

export default calendarReducer;