import {createSelector} from 'reselect';

const selectCalendar = state => state.calendar;

export const selectViewingsOffCalendar = createSelector(
    [selectCalendar],
    calendar => (calendar.viewings ? calendar.viewings : [])  
);