import {createSelector} from 'reselect';

const selectRooms = state => state.rooms;

export const selectRoomList = createSelector(
    [selectRooms],
    rooms => rooms
)