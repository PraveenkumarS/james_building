import RoomActionTypes from './room.types';

const INITIAL_STATE = {
  rooms: null,
  isFetching: false,
  errorMessage: undefined
};

const roomReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RoomActionTypes.FETCH_ROOMS_START:
      return {
        ...state,
        isFetching: true
      };
    case RoomActionTypes.FETCH_ROOMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        rooms: action.payload
      };
    case RoomActionTypes.FETCH_ROOMS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};

export default roomReducer;