import RoomActionTypes from './room.types';

export const fetchRoomsStart = () => ({
  type: RoomActionTypes.FETCH_ROOMS_START
});

export const fetchRoomsSuccess = roomsMap => ({
  type: RoomActionTypes.FETCH_ROOMS_SUCCESS,
  payload: roomsMap
});

export const fetchRoomsFailure = errorMessage => ({
  type: RoomActionTypes.FETCH_ROOMS_FAILURE,
  payload: errorMessage
});