import { takeLatest, call, put, all } from 'redux-saga/effects';

import { firestore } from '../../firebase/firebase';

import { fetchRoomsSuccess, fetchRoomsFailure } from './room.actions';

import RoomActionTypes from './room.types';

export function* fetchRoomsAsync() {
  try {
    const roomRef = firestore.collection('rooms');
    const snapshot = yield roomRef.get();
    let fetchedRooms = [];
        for (let i = 0; i < snapshot.docs.length; i++) {
            let roomQuery = {
              ...snapshot.docs[i].data(),
              id: snapshot.docs[i].id
            };
            fetchedRooms.push(roomQuery);
        }
    yield put(fetchRoomsSuccess(fetchedRooms));
  } catch (error) {
    yield put(fetchRoomsFailure(error.message));
  }
}

export function* fetchRoomsStart() {
  yield takeLatest(
    RoomActionTypes.FETCH_ROOMS_START,
    fetchRoomsAsync
  );
}

export function* roomSagas() {
  yield all([call(fetchRoomsStart)]);
}