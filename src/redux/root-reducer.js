import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import buildingReducer from './buildings/building.reducer';
import roomReducer from './rooms/room.reducer';
import userReducer from './users/user.reducer';
import generalReducer from './general/general.reducer';
import loadingReducer from './loading/loading.reducer';
import {reducer as ToastrReducer} from 'react-redux-toastr'
import calendarReducer from './calendar/calendar.reducer';


const persistConfig = {
  key: 'root',
  storage
};

const appReducer = combineReducers({
  buildings: buildingReducer,
  calendar: calendarReducer,
  general: generalReducer,
  loading: loadingReducer,
  rooms: roomReducer,
  toastr: ToastrReducer,
  user: userReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'SIGN_OUT_SUCCESS') {
    state = undefined
  }
  return appReducer(state, action)
}

export default persistReducer(persistConfig, rootReducer);