import {createSelector} from 'reselect';

const selectGeneral = state => state.general;

export const selectCountriesOffState = createSelector(
    [selectGeneral],
    countries => (countries.countries ? countries.countries : [])  
);

export const selectBuildingTypesOffState = createSelector(
    [selectGeneral],
    building_types => (building_types.building_types ? building_types.building_types : [])  
);
