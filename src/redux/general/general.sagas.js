import { takeLatest, call, put, all } from 'redux-saga/effects';
import { firestore } from '../../firebase/firebase';
import { 
    countriesDocumentSuccess,
    countriesDocumentFailure,
    buildingTypesDocumentSuccess,
    buildingTypesDocumentFailure
} from './general.actions';
import GeneralActionTypes from './general.types';


export function* countriesDocumentAsync() {
  try {
    let countriesDocRef = firestore.collection('general').doc('countries');
    let doc = yield countriesDocRef.get();
    if (doc.exists) {
      let snapshotData = doc.data();
      console.log("snapshotData",snapshotData)
      yield put(countriesDocumentSuccess(snapshotData));
    } else {
      yield put(countriesDocumentFailure('Doc not found'));
    }    
  } catch (error) {
      yield put(countriesDocumentFailure(error.message));
  }
}

export function* buildingTypesDocumentAsync() {
  try {
    let countriesDocRef = firestore.collection('general').doc('building_types');
    let doc = yield countriesDocRef.get();
    if (doc.exists) {
      let snapshotData = doc.data();
      yield put(buildingTypesDocumentSuccess(snapshotData));
    } else {
      yield put(buildingTypesDocumentFailure('Doc not found'));
    }    
  } catch (error) {
      yield put(buildingTypesDocumentFailure(error.message));
  }
}


export function* countriesDocumentStart() {
  yield takeLatest(
    GeneralActionTypes.COUNTRIES_DOCUMENT_REQUEST,
    countriesDocumentAsync
  );
}

export function* buildingTypesDocumentStart() {
  yield takeLatest(
    GeneralActionTypes.BUILDING_TYPES_DOCUMENT_REQUEST,
    buildingTypesDocumentAsync
  );
}

export function* generalSagas() {
  yield all([
    call(countriesDocumentStart),
    call(buildingTypesDocumentStart)
  ]);
}

