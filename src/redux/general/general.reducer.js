import GeneralActionTypes from './general.types';

const INITIAL_STATE = {
  countries: null,
  building_types: null,
  errorMessage: undefined
};

const generalReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GeneralActionTypes.COUNTRIES_DOCUMENT_REQUEST:
    case GeneralActionTypes.BUILDING_TYPES_DOCUMENT_REQUEST:
      return {
        ...state
      };

    case GeneralActionTypes.COUNTRIES_DOCUMENT_SUCCESS:
      return {
        ...state,
        countries: action.payload
      };

    case GeneralActionTypes.BUILDING_TYPES_DOCUMENT_SUCCESS:
      return {
        ...state,
        building_types: action.payload
      };

    case GeneralActionTypes.COUNTRIES_DOCUMENT_FAILURE:
    case GeneralActionTypes.BUILDING_TYPES_DOCUMENT_REQUEST_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    default:
      return state;
  }
};

export default generalReducer;