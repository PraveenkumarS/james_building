import GeneralActionTypes from './general.types';

export const getCountryList = () => ({
  type: GeneralActionTypes.COUNTRIES_DOCUMENT_REQUEST
});

export const countriesDocumentSuccess = docData => ({
  type: GeneralActionTypes.COUNTRIES_DOCUMENT_SUCCESS,
  payload: docData
});

export const countriesDocumentFailure = errorMessage => ({
  type: GeneralActionTypes.COUNTRIES_DOCUMENT_FAILURE,
  payload: errorMessage
});

export const getBuildingTypes = () => ({
  type: GeneralActionTypes.BUILDING_TYPES_DOCUMENT_REQUEST
});

export const buildingTypesDocumentSuccess = docData => ({
  type: GeneralActionTypes.BUILDING_TYPES_DOCUMENT_SUCCESS,
  payload: docData
});

export const buildingTypesDocumentFailure = errorMessage => ({
  type: GeneralActionTypes.BUILDING_TYPES_DOCUMENT_FAILURE,
  payload: errorMessage
});
