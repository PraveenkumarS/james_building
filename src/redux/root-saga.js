import { all, call } from 'redux-saga/effects';
import { roomSagas } from './rooms/room.sagas';
import { userSagas } from './users/user.sagas';
import { buildingSagas } from './buildings/building.sagas';
import { generalSagas } from './general/general.sagas';
import { calendarSagas } from './calendar/calendar.sagas';


export default function* rootSaga() {
    yield all([call(userSagas), call(roomSagas), call(buildingSagas), call(generalSagas), call(calendarSagas)]);
}