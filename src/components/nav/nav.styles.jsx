import styled from 'styled-components';
import { Menu } from 'semantic-ui-react';

export const HorizontalNav = styled(Menu)`
  &.ui.fixed.menu {
      background: transparent;
      box-shadow: none;
      border: none;
      height: 80px;

      .right.menu {
        .item:before {
          background: transparent;
        }
  
        .item {
          margin-right: 18px;

          &.nav-notifications {
            margin: 0;
            padding: 0;

            i.icon {
              font-size: 24px;
              color: ${props => props.theme.white};
              border: 2px solid ${props => props.theme.white};
              border-radius: 50%;
              width: 40px;
              height: 40px;
              color: ${props => props.theme.red};
              background: ${props => props.theme.white};

              &:before {
                position: relative;
                top: 7px;
              }
            }
          }
        }
  
        img {
          width: 52px;
          height: 52px;
          margin-left: 10px;
          border: 2px solid #fff;
          border-radius: 50%;
          position: relative;
          top: 1px;
      }
  
      .dropdown {
          .text {
              color: #fff;
              line-height: 72px;
          }
      }
      }
  }
`

export const VerticalNavWrapper = styled.div`
  .ui.vertical.accordion.ui.menu {
    overflow-y: scroll;
    height: 100vh;
    background: ${props => props.theme.white};
    position: fixed;
    top: 0;
    left: 0;
    border: none;
    z-index: 1000;
    width: 250px;
    margin: 0;
    border-radius: 0;
    box-shadow: 0 0 2rem 0 rgba(136,152,170,.15);

    .logo {
      text-align: center;
      border-bottom: 1px solid ${props => props.theme.grey};
    }

    .menu-items {
      padding: 0 8px;

      .item {
        color: ${props => props.theme.black};
        letter-spacing: 1px;
        font-size: 15px;
        margin: 0;
        padding: 20px 10px;
        border-bottom: 1px solid ${props => props.theme.grey};

        i.icon {
          float: none;
          margin-right: 8px;
          color: ${props => props.theme.green};
          font-size: 16px;

          &.chevron {
            float: right;
            color: rgba(0, 0, 0, 0.7);
            font-size: 9px;
            position: relative;
            top: 6px;
          }
        }

        .content {
          padding-top: 20px;

          .item {
            border: none;
          }
        }
      }
    }
  }
`

export const SearchBarWrapper = styled.div`
  position: absolute;
  right: 200px;
  height: 48px;
  margin: 16px 0 0 16px;

  form {
    position: relative;

    .search-input {
      -webkit-appearance: none;
      border: 1px solid ${props => props.theme.white};
      background-image: none;
      background-color: transparent;
      -webkit-box-shadow: none;
      -moz-box-shadow: none;
      box-shadow: none;
      padding: 16px 12px 14px 48px;
      width: 300px;
      border-radius: 3px;
      height: 48px;
      color: ${props => props.theme.white};
    }

    .buttons {
      position: absolute;
      top: 14px;
      left: 8px;

      button {
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
        color: ${props => props.theme.white};
        font-size: 18px;
      }
    }

    select {
      font-size: 16px;
      font-family: sans-serif;
      line-height: 1.3;
      padding: 12px 6px;
      box-sizing: border-box;
      margin: 0;
      box-shadow: none;
      border-radius: 3px;
      height: 39px;
      position: relative;
      top: -1px;
      background: transparent;
      border: 1px solid #fff;
      color: #fff;
}
    }

    .select-css::-ms-expand {
      display: none;
    }
    .select-css:hover {
        border-color: #888;
    }
    .select-css:focus {
        border-color: #aaa;
        box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
        box-shadow: 0 0 0 3px -moz-mac-focusring;
        color: #222; 
        outline: none;
    }
    .select-css option {
        font-weight:normal;
    }
  }

  input::-webkit-input-placeholder {
    color: #fff;
    font-size: 16px;
  }

  input:-moz-placeholder {
    color: #fff;
    font-size: 16px;
  }

  input::placeholder {
    color: #fff;
    font-size: 16px;
  }

  input::-moz-placeholder {
    color: #fff;
    font-size: 16px;
  }

  input::-ms-input-placeholder {
    color: #fff;
    font-size: 16px;
  }

  input:-ms-input-placeholder {
    color: #fff;
    font-size: 16px;
  }
`