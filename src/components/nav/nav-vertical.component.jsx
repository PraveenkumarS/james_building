import React, { useState } from 'react';
import { NavLink, withRouter, Link } from 'react-router-dom';
import { Accordion, Icon, Menu } from 'semantic-ui-react';
import { VerticalNavWrapper } from './nav.styles';

import { ReactComponent as Logo } from '../../app/assets/logo.svg';

const VerticalNav = () => {
  const [activeIndex, setActiveIndex] = useState({ positionCount: 0 });

  const handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex.positionCount === index ? -1 : index;
    setActiveIndex({ ...activeIndex, positionCount: newIndex });
  };

  return (
    <VerticalNavWrapper>
      <Accordion as={Menu} vertical text>
        <div className="logo">
          <Link to="/">
            <Logo
              src="/assets/images/NBHLogoBlackSmall.png"
              alt="Logo"
            />
          </Link>
        </div>

        <div className="menu-items">
        <Menu.Item as={NavLink} to="/" name="dashboard">
          <Icon name="block layout" />
          Dashboard
        </Menu.Item>

        <Menu.Item as={NavLink} to="/calendar" name="calendar">
          <Icon name="calendar alternate outline" />
          Calendar
        </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 1}
              index={1}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="user outline" />
              Users
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 1}>
              <Menu.Item as={NavLink} to="/users" name="users">
                <Icon name="user outline" />
                Users
              </Menu.Item>
              <Menu.Item as={NavLink} to="/payments" name="payments">
                <Icon name="payment" />
                Payments
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 2}
              index={2}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="building outline" />
              Properties
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 2}>
              <Menu.Item as={NavLink} to="/buildings" name="buildings">
                <Icon name="building outline" />
                Buildings
              </Menu.Item>
              <Menu.Item as={NavLink} to="/units" name="units">
                <Icon name="home" />
                Units
              </Menu.Item>
              <Menu.Item as={NavLink} to="/rooms" name="rooms">
                <Icon name="bed" />
                Rooms
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item as={NavLink} to="/viewings" name="viewings">
            <Icon name="eye" />
            Viewings
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 3}
              index={3}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="file alternate outline" />
              Applications
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 3}>
              <Menu.Item as={NavLink} to="/housemate-approvals" name="housemate-approvals">
                <Icon name="like" />
                Match Approvals
              </Menu.Item>
              <Menu.Item as={NavLink} to="/member-applications" name="member-applications">
                <Icon name="file alternate outline" />
                Member Applications
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 4}
              index={4}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="tasks" />
              Tasks
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 4}>
              <Menu.Item as={NavLink} to="/tasks-cleaning" name="tasks-cleaning">
                <Icon name="trash" />
                Cleaning
              </Menu.Item>
              <Menu.Item as={NavLink} to="/tasks-maintenance" name="tasks-maintenance">
                <Icon name="wrench" />
                Maintenance
              </Menu.Item>
              <Menu.Item as={NavLink} to="/tasks-meter-readings" name="tasks-meter-readings">
                <Icon name="tachometer alternate" />
                Meter Readings
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item as={NavLink} to="/partners" name="partners">
            <Icon name="handshake outline" />
            Partners
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 5}
              index={5}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="newspaper outline" />
              Blog
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 5}>
              <Menu.Item as={NavLink} to="/users" name="posts">
                <Icon name="conversation" />
                Posts
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="tags">
                <Icon name="tags" />
                Tags
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 6}
              index={6}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="feed" />
              Feed
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 6}>
              <Menu.Item as={NavLink} to="/events" name="events">
                <Icon name="cocktail" />
                Events
              </Menu.Item>
              <Menu.Item as={NavLink} to="/status-updates" name="status-updates">
                <Icon name="comment" />
                Status Updates
              </Menu.Item>
              <Menu.Item as={NavLink} to="/help-requests" name="help-requests">
                <Icon name="help" />
                Help Requests
              </Menu.Item>
              <Menu.Item as={NavLink} to="/tags" name="tags">
                <Icon name="tags" />
                Tags
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 7}
              index={7}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="sync" />
              Activity
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 7}>
              <Menu.Item as={NavLink} to="/user-notifications" name="user-notifications">
                <Icon name="users" />
                User Activity
              </Menu.Item>
              <Menu.Item as={NavLink} to="/admin-notifications" name="admin-notifications">
                <Icon name="chess queen" />
                Admin Activity
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 8}
              index={8}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="bell" />
              Notifications
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 8}>
              <Menu.Item as={NavLink} to="/user-notifications" name="user-notifications">
                <Icon name="users" />
                Users
              </Menu.Item>
              <Menu.Item as={NavLink} to="/admin-notifications" name="admin-notifications">
                <Icon name="chess queen" />
                Admin
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item as={NavLink} to="/reports" name="reports">
            <Icon name="line graph" />
            Reports
          </Menu.Item>
        </div>
      </Accordion>
    </VerticalNavWrapper>
  );
};

export default withRouter(VerticalNav);

/*
class VerticalNav extends Component {

    state = { activeIndex: 0 }
  
    handleClick = (e, titleProps) => {
      const { index } = titleProps
      const { activeIndex } = this.state
      const newIndex = activeIndex === index ? -1 : index
  
      this.setState({ activeIndex: newIndex })
    }
  
    render() {
      const { activeIndex } = this.state
  
      return (
        <VerticalNavWrapper>
          <Accordion as={Menu} vertical text>
            <Image as={Link} to="/" src="/assets/images/NBHLogoBlackSmall.png" alt="Logo" className="logo" />
            <div>
              <Menu.Item>
                <Accordion.Title
                  active={activeIndex === 1}
                  index={1}
                  onClick={this.handleClick}
                >
                  <Icon name='chevron down' />
                  <Icon name='user outline' />Users
                </Accordion.Title>          
                <Accordion.Content active={activeIndex === 1}>
                  <Menu.Item as={NavLink} to="/users" name="users"><Icon name='user outline' />Users</Menu.Item>
                  <Menu.Item as={NavLink} to="/member-applications" name="member-applications"><Icon name='file alternate outline' />Applications</Menu.Item>
                  <Menu.Item as={NavLink} to="/payments" name="payments"><Icon name='payment' />Payments</Menu.Item>
                </Accordion.Content>
              </Menu.Item>
  
              <Menu.Item>
                <Accordion.Title
                  active={activeIndex === 2}
                  index={2}
                  onClick={this.handleClick}
                >
                  <Icon name='chevron down' />
                  <Icon name='building outline' />Properties
                </Accordion.Title>          
                <Accordion.Content active={activeIndex === 2}>
                  <Menu.Item as={NavLink} to="/users" name="users"><Icon name='building outline' />Buildings</Menu.Item>
                  <Menu.Item as={NavLink} to="/rooms" name="rooms"><Icon name='bed' />Rooms</Menu.Item>
                  <Menu.Item as={NavLink} to="/users" name="viewings"><Icon name='eye' />Viewings</Menu.Item>
                  <Menu.Item as={NavLink} to="/room-applications" name="room-applications"><Icon name='file alternate outline' />Applications</Menu.Item>
                </Accordion.Content>
              </Menu.Item>
  
              <Menu.Item>
                <Accordion.Title
                  active={activeIndex === 3}
                  index={3}
                  onClick={this.handleClick}
                >
                  <Icon name='chevron down' />
                  <Icon name='newspaper outline' />Blog
                </Accordion.Title>          
                <Accordion.Content active={activeIndex === 3}>
                  <Menu.Item as={NavLink} to="/users" name="applications"><Icon name='conversation' />Posts</Menu.Item>
                  <Menu.Item as={NavLink} to="/users" name="viewings"><Icon name='tags' />Tags</Menu.Item>
                </Accordion.Content>
              </Menu.Item>
  
              <Menu.Item>
                <Accordion.Title
                  active={activeIndex === 4}
                  index={4}
                  onClick={this.handleClick}
                >
                  <Icon name='chevron down' />
                  <Icon name='cocktail' />Events
                </Accordion.Title>          
                <Accordion.Content active={activeIndex === 4}>
                  <Menu.Item as={NavLink} to="/users" name="applications"><Icon name='cocktail' />Events</Menu.Item>
                  <Menu.Item as={NavLink} to="/users" name="viewings"><Icon name='tags' />Tags</Menu.Item>
                </Accordion.Content>
              </Menu.Item>
  
              <Menu.Item>
                <Accordion.Title
                  active={activeIndex === 5}
                  index={5}
                  onClick={this.handleClick}
                >
                  <Icon name='chevron down' />
                  <Icon name='facebook messenger' />Messages
                </Accordion.Title>          
                <Accordion.Content active={activeIndex === 5}>
                  <Menu.Item as={NavLink} to="/users" name="applications"><Icon name='plus' />Help</Menu.Item>
                  <Menu.Item as={NavLink} to="/users" name="viewings"><Icon name='wrench' />Maintenance</Menu.Item>
                  <Menu.Item as={NavLink} to="/users" name="viewings"><Icon name='trash' />Cleaning</Menu.Item>
                </Accordion.Content>
              </Menu.Item>
            </div>
          </Accordion>
        </VerticalNavWrapper>
      );
    }
  }
  
  export default withRouter(VerticalNav);
  

   <VerticalNavWrapper>
      <Accordion as={Menu} vertical text>
        <div className="logo">
          <Link to="/">
            <Logo
              src="/assets/images/NBHLogoBlackSmall.png"
              alt="Logo"
            />
          </Link>
        </div>
        <div className="menu-items">
          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 1}
              index={1}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="user outline" />
              Users
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 1}>
              <Menu.Item as={NavLink} to="/users" name="users">
                <Icon name="user outline" />
                Users
              </Menu.Item>
              <Menu.Item
                as={NavLink}
                to="/member-applications"
                name="member-applications"
              >
                <Icon name="file alternate outline" />
                Applications
              </Menu.Item>
              <Menu.Item as={NavLink} to="/payments" name="payments">
                <Icon name="payment" />
                Payments
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 2}
              index={2}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="building outline" />
              Properties
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 2}>
              <Menu.Item as={NavLink} to="/buildings" name="buildings">
                <Icon name="building outline" />
                Buildings
              </Menu.Item>
              <Menu.Item as={NavLink} to="/rooms" name="rooms">
                <Icon name="bed" />
                Rooms
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="viewings">
                <Icon name="eye" />
                Viewings
              </Menu.Item>
              <Menu.Item
                as={NavLink}
                to="/room-applications"
                name="room-applications"
              >
                <Icon name="file alternate outline" />
                Applications
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 3}
              index={3}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="newspaper outline" />
              Blog
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 3}>
              <Menu.Item as={NavLink} to="/users" name="applications">
                <Icon name="conversation" />
                Posts
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="viewings">
                <Icon name="tags" />
                Tags
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 4}
              index={4}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="cocktail" />
              Events
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 4}>
              <Menu.Item as={NavLink} to="/users" name="applications">
                <Icon name="cocktail" />
                Events
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="viewings">
                <Icon name="tags" />
                Tags
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>

          <Menu.Item>
            <Accordion.Title
              active={activeIndex.positionCount === 5}
              index={5}
              onClick={handleClick}
            >
              <Icon name="chevron down" />
              <Icon name="facebook messenger" />
              Messages
            </Accordion.Title>
            <Accordion.Content active={activeIndex.positionCount === 5}>
              <Menu.Item as={NavLink} to="/users" name="applications">
                <Icon name="plus" />
                Help
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="viewings">
                <Icon name="wrench" />
                Maintenance
              </Menu.Item>
              <Menu.Item as={NavLink} to="/users" name="viewings">
                <Icon name="trash" />
                Cleaning
              </Menu.Item>
            </Accordion.Content>
          </Menu.Item>
        </div>
      </Accordion>
    </VerticalNavWrapper>
*/
