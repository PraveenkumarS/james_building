import React from 'react';
import { Form, Field } from 'react-final-form';
import { Icon } from 'semantic-ui-react';
import { SearchBarWrapper } from './nav.styles';

const NavSearch = () => {

    const handleSubmit = async values => {
        console.log(values);
    };

    return (
        <SearchBarWrapper>
            <Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                    <form onSubmit={handleSubmit}>
                        <div className="buttons">
                            <button type="submit" disabled={submitting || pristine}>
                                <Icon name="search" />
                            </button>
                        </div>
                        <Field
                            className="search-input"
                            name="navSearchQuery"
                            component="input"
                            type="text"
                            placeholder="Search here..."
                        />
                    </form>
                )}
            />
        </SearchBarWrapper>
    );
}

export default NavSearch;

/*
<Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                    <form onSubmit={handleSubmit}>
                        <div className="buttons">
                            <button type="submit" disabled={submitting || pristine}>
                                <Icon name="search" />
                            </button>
                        </div>
                        <Field
                            className="search-input"
                            name="navSearchQuery"
                            component="input"
                            type="text"
                            placeholder="Search here..."
                        />
                        <Field name="searchFieldSelected" component="select" defaultValue="users">
                            <option value="users">Users</option>
                            <option value="city">Buildings</option>
                            <option value="units">Units</option>
                            <option value="rooms">Rooms</option>
                            <option value="viewings">Viewings</option>
                        </Field>
                    </form>
                )}
            />
*/