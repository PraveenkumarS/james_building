import React from 'react';
import { connect } from 'react-redux';
import { Menu, Dropdown, Icon } from 'semantic-ui-react';
import { HorizontalNav } from './nav.styles';
import { signOutStart } from '../../redux/users/user.actions';
import NavSearch from './nav-search.component';

const mapDispatch = dispatch => ({
  signOutStart: () => dispatch(signOutStart())
});

const NavBar = ({ currentUser, signOutStart }) => {
  return (
    <HorizontalNav fixed="top">
      <NavSearch />
      <Menu.Menu position="right">
        <Menu.Item className="nav-notifications">
          <Icon name="bell" />
        </Menu.Item>
        <Menu.Item>
          <Dropdown 
            pointing="top left"
            icon={<img alt="profile pic" src={currentUser.photoURL || '/assets/images/user-icon.png'} />}
          >
            <Dropdown.Menu>
              <Dropdown.Item text="Sign Out" onClick={signOutStart} />
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Item>
      </Menu.Menu>
    </HorizontalNav>
  );
};

export default connect(null, mapDispatch)(NavBar);