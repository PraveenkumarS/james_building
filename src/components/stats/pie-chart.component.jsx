import React, { PureComponent } from 'react';
import { PieChart, Pie, Legend } from 'recharts';
import { PieChartContainer } from './stats.styles';

const data = [
    { name: 'Up to 1 week', value: 50, fill: 'purple' }, 
    { name: '1-4 weeks', value: 90, fill: 'orange' }, 
    { name: '1-3 months', value: 150, fill: 'red' }, 
    { name: '3-6 months', value: 190, fill: 'green' },
    { name: '6 months +', value: 520, fill: 'blue' }
];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index
}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

class PieChartGraph extends PureComponent {
    render() {
        return (
            <PieChartContainer>
                <h2>Length of stay:</h2>
                <PieChart width={440} height={440}>
                    <Pie
                        data={data}
                        cx={220}
                        cy={220}
                        labelLine={false}
                        label={renderCustomizedLabel}
                        outerRadius={200}
                        fill="fill"
                        dataKey="value"
                    >
                    </Pie>
                    <Legend iconSize={10} width={180} height={40} layout="horizontal" verticalAlign="middle" />
                </PieChart>
            </PieChartContainer>
        );
    }
}

export default PieChartGraph;