import styled from 'styled-components';
import { Card } from 'semantic-ui-react';

export const CardComponent = styled(Card)`
    border-radius: 5px !important;
    width: 100% !important;
    
    &.no-shadow {
        box-shadow: none;
    }

    i.circular {
        float: right;
        font-size: 26px;
    }
    .content {
        padding: 24px !important;

        .header {
            font-size: 16px !important;
            color: ${props => props.theme.medGrey} !important;
        }
        .meta {
            font-size: 24px;
            color: ${props => props.theme.black};
            font-weight: 900;
            margin: 10px 0;
            letter-spacing: 1px;
        }
        .description {
            margin: 18px 0 0 !important;

            span {
                color: ${props => props.theme.green};
            }
        }
    }
`

export const GraphContainer = styled.div`
    width: 100%;
    height: 460px;
    padding: 60px 36px 24px 0;
    background: linear-gradient(87deg,#172b4d,#1a174d);
    border-radius: 5px;
    box-shadow: 0 0 2rem 0 rgba(136,152,170,.15);


    .recharts-yAxis.yAxis, .recharts-xAxis.xAxis {
        stroke-opacity: 0;
    }

    .recharts-layer {
        text {
          fill: rgba(255,255,255,.6);
          tspan {
            font-size: 16px;
          }
        }
    }

    .graph-title {
        font-size: 19px;
        color: #fff;
        font-weight: 900;
        display: block;
        position: absolute;
        top: 24px;
        left: 40px;
    }   
      
`

export const PieChartContainer = styled.div`

    background: ${props => props.theme.white};
    padding: 24px;
    border-radius: 5px;
    box-shadow: 0 0 2rem 0 rgba(136,152,170,.15);

    h2 {
        font-size: 24px;
        color: #000;
        font-weight: 900;
        letter-spacing: 1px;
        padding-bottom: 12px;
        margin-bottom: 16px;
        line-height: 1;
        border-bottom: 1px solid ${props => props.theme.grey};
    }

    .recharts-wrapper {
        margin: auto;
        position: relative;
        left: -3px;
    }

 .recharts-legend-wrapper {
     position: absolute;
     top: auto !important;
     right: -36px !important;
     bottom: -14px !important;
     left: auto !important;
 }
`
