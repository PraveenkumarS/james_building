import React, { PureComponent } from 'react';
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
} from 'recharts';
import { GraphContainer } from './stats.styles';

const data = [
  {
    name: 'Wk 1',
    users: 70
  },
  {
    name: 'Wk 2',
    users: 73
  },
  {
    name: 'Wk 3',
    users: 77
  },
  {
    name: 'Wk 4',
    users: 82
  },
  {
    name: 'Wk 5',
    users: 88
  },
  {
    name: 'Wk 6',
    users: 95
  }
];

class Graph extends PureComponent {
  render() {
    
    return (
      <GraphContainer>
        <span className="graph-title">Occupany WoW</span>
        <ResponsiveContainer>
          <LineChart
            data={data}
            title="Chart of PU x UV"
          >
            <XAxis dataKey="name" dy={12} dx={-6} />
            <YAxis dx={-12} />
            <Tooltip />
            <Line
              type="monotone"
              dataKey="users"
              stroke="#ffffff"
              strokeWidth="4"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </ResponsiveContainer>
      </GraphContainer>
    );
  }
}

export default Graph;
