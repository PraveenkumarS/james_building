import React from 'react';
import { Card, Icon } from 'semantic-ui-react';
import { CardComponent } from './stats.styles';

const SingleCard = ({collection, stats, uplift, icon, color}) => {
    return (
        <CardComponent className="no-shadow">
          <Card.Content>
            <Icon
              className="top"
              circular
              inverted
              color={color}
              name={icon}
            />
            <Card.Header>{collection}</Card.Header>
            <Card.Meta>{stats}</Card.Meta>
            <Card.Description>
              <span><Icon name="arrow up" />{uplift}%</span> Since last week
            </Card.Description>
          </Card.Content>
        </CardComponent>
    );
}

export default SingleCard;