import React from 'react';
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import {emailSignInStart} from '../../redux/users/user.actions';

const mapDispatch = dispatch => ({
  emailSignInStart: (email, password) => dispatch(emailSignInStart({email, password}))
})

const SignInForm = ({emailSignInStart}) => {

  const handleSubmit = async values => {
    // const { emailSignInStart } = this.props;
    emailSignInStart(values.email, values.password);
  };

  return (
    <div className='sign-in'>
      <Form
        onSubmit={handleSubmit}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit}>
            
            <Field
              name="email"
              component="input"
              type="email"
              placeholder="Email"
            />
            <Field
              name="password"
              component="input"
              type="password"
              placeholder="Password"
            />
            <div className="buttons">
              <button type="submit" disabled={submitting || pristine}>
                Submit
              </button>
            </div>
          </form>
        )}
      />
    </div>
  )
}

export default connect(null, mapDispatch)(SignInForm);



/*
Validation
Limit login attempts


class SignInForm extends React.Component {
    state = {
        email: '',
        password: ''
      };

      handleSubmit = event => {
        event.preventDefault();

        const { email, password } = this.state;
        console.log(this.state);
        // this.setState({ email: '', password: '' });
      };
    
      handleChange = event => {
        const { value, name } = event.target;
    
        this.setState({ [name]: value });
      };
      
      render() {
        return (
            <div className='sign-in'>
            <form onSubmit={this.handleSubmit}>
              <FormInput
                name='email'
                type='email'
                handleChange={this.handleChange}
                value={this.state.email}
                label='email'
                required
              />
              <FormInput
                name='password'
                type='password'
                value={this.state.password}
                handleChange={this.handleChange}
                label='password'
                required
              />
              <div className='buttons'>
                <CustomButton type='submit'> Sign in </CustomButton>
              </div>
            </form>
          </div>
        )
      }
    
}






*/