import React from 'react';
import { Grid } from 'semantic-ui-react';
import { PageDashboardContainer } from './page-dashboard.styles';
import PageDashFilters from './page-dash-filters.component';
import PageDashResults from './page-dash-results.component';

const PageDashboard = ({filters, filterTitles, initialFetched, applyFilters, columns}) => {
    return (
        <PageDashboardContainer>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={3}>
                        <PageDashFilters filters={filters} filterTitles={filterTitles} applyFilters={applyFilters} />
                    </Grid.Column>
                    <Grid.Column width={13}>
                        <PageDashResults initialFetched={initialFetched} columns={columns} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </PageDashboardContainer>
    );
}

export default PageDashboard;