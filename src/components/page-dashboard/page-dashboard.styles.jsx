import styled from 'styled-components';

export const PageDashboardContainer = styled.div`
    background: ${props => props.theme.white};

    .ui.grid {
        margin: 0;

        > .row {
          padding-top: 0;
          padding-bottom: 20px;

          .column {
              padding: 0;
          }
        }
    }

    .page-dash-filters-form {
        .filters-section {
            padding: 18px;
            border-right: 1px solid ${props => props.theme.grey};
            border-bottom: 1px solid ${props => props.theme.grey};
    
            h2 {
                font-size: 20px;
            }
            h3 {
                font-size: 16px;
                text-transform: capitalize;
            }

            input[type="radio"] {
                opacity: 0;
                position: absolute;
                display: inline-block;
                vertical-align: middle;
                margin: 5px;
                cursor: pointer;
            }

            label {
                display: inline-block;
                vertical-align: middle;
                margin: 5px;
                cursor: pointer;
                position: relative;
                text-transform: capitalize;
            }

            input[type="radio"] + label:before {
                content: '';
                background: #fff;
                border: 2px solid #eee;
                display: inline-block;
                vertical-align: middle;
                width: 20px;
                height: 20px;
                padding: 2px;
                margin-right: 10px;
                text-align: center;
                background-color: #eee;
                position: relative;
                top: -1px;
            }

            input[type="radio"] + label:hover:before {
                background-color: #ccc;
                border: 2px solid #ccc;
            }

            label:after {
                content: "";
                position: absolute;
                display: none;
            }

            input[type="radio"]:checked + label:after {
                left: 7px;
                top: 4px;
                width: 6px;
                height: 11px;
                border: solid #fff;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
                display: block;
            }

            input[type="radio"]:checked + label:before {
                background-color: #2196F3;
                border: 2px solid #2196F3;
            }
            
        }
        .button {
            background: ${props => props.theme.green};
            color: ${props => props.theme.white};
            width: 100%;
            border-radius: 0;
            padding: 16px;
        }
    }

    .page-results-table {
        .table {
            border-radius: 0;
            border: none;
            border-bottom: 1px solid rgba(34,36,38,.1);

            th {
                padding: 21px 12px;
                text-transform: capitalize !important;
                border-right: none !important;
                border-left: none !important;
            }
            td {
                padding: 16px 12px;
                border-right: none !important;
                border-left: none !important;
                text-transform: capitalize;
            }
        }
    }
`

export const PageHeader = styled.div`
    display: flex;
        padding: 36px 0;

        .copy {
            display: flex;
            padding: 0 36px;
            border-right: 1px solid ${props => props.theme.white};

            .icon {
                font-size: 28px;
                border-radius: 50%;
                width: 50px;
                height: 50px;
                color: ${props => props.theme.darkBlue};
                background: ${props => props.theme.white};
                margin-top: 2px;
    
                &:before {
                    position: relative;
                    top: 15px;
                }
            }
    
            .header {
                margin-left: 12px;
    
                span {
                    color: ${props => props.theme.white};
                    font-size: 11px;
                    text-transform: uppercase;
                    letter-spacing: 1px;
                }
                h1 {
                    color: ${props => props.theme.white};
                    font-size: 26px;
                    margin: 0;
                }
            }

            &.dashboard {
                .header {
                    h1 {
                        margin-top: 11px;
                    }
                }
            }
            &.form {
                border-right: none;
                
                .header {
                    h1 {
                        margin-top: 11px;
                    }
                }
            }
        }

        .stats {
            padding: 0 36px;
            display: flex;

            .stat {
                &.first {
                    margin-right: 24px;
                }

                span.title {
                    color: ${props => props.theme.white};
                    font-size: 11px;
                    text-transform: uppercase;
                    letter-spacing: 1px;
                    display: block;
                }
                span.number {
                    color: ${props => props.theme.white};
                    font-size: 26px;
                    margin-top: 12px;
                    display: block;
                    font-weight: 900;
                }
            }
        }

        .buttons {
            padding: 0 36px 0 0;
            margin-left: auto;
            order: 2;

            .create {
                background: ${props => props.theme.white};
                color: ${props => props.theme.black};
            }

            .edit {
                background: ${props => props.theme.white};
                color: ${props => props.theme.black};
            }
            .delete {
                background: #fff;
                padding: 8px;
                border-radius: 3px;
            }
        }
`