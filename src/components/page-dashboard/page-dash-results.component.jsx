import React from 'react';
import PageDashResultsItem from './page-dash-results-item.component';
import { Table } from 'semantic-ui-react';

const PageDashResults = ({initialFetched, columns}) => {
    return (
        <div className="page-results-table">
            <Table celled striped>
                <Table.Header>
                    <Table.Row>
                        {columns.map((column, index) => (
                            <Table.HeaderCell key={index}>{column}</Table.HeaderCell>
                        ))}
                    </Table.Row>
                </Table.Header>
                    <Table.Body>
                        {initialFetched.length ? (
                            initialFetched.map((item, index) => (
                                <PageDashResultsItem key={index} item={item} />
                            ))
                        ) : (
                        <Table.Row>
                            <Table.Cell>No results</Table.Cell>
                            </Table.Row>
                        )}
                    </Table.Body>
            </Table>
        </div>
    );
}

export default PageDashResults;