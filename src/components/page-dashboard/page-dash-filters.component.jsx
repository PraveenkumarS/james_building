import React from 'react';
import { Form, Field } from 'react-final-form';
import { Button } from 'semantic-ui-react';

const PageDashFilters = ({filters, filterTitles, applyFilters}) => {

    const handleSubmit = async values => {
        applyFilters(values);
    };

    console.log(filterTitles)
    return (
        <div className="page-dash-filters-form">
            <div className="filters-section">
                <h2>Filters</h2>
            </div>
            
            <Form
                onSubmit={handleSubmit}
                render={({ handleSubmit, form, submitting, pristine, values }) => (
                <form onSubmit={handleSubmit}>                
                    
                        <div className="filters-section">
                            <h3>{filterTitles[0]}</h3>
                            {Object.keys(filters).map((value, subIndex) => (
                                <div key={subIndex}>
                                    <label>{value}
                                        <Field 
                                            name={value} 
                                            component="input" 
                                            type="checkbox"
                                            value={value}
                                        />
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                            ))};
                        </div>
                    }

                    <Button type="submit" disabled={submitting || pristine}>
                        Filter now
                    </Button>
                </form>
                )}
            />
        </div>
    );
}

export default PageDashFilters;

/*
{filters.map((filter, index) => {
                    return (
                        <div key={index}>
                            <label>{filter}</label>
                            <Field 
                                name={filter} 
                                component="input" 
                                type="checkbox"
                            />
                        </div>
                    )
                })}


{filters.map((filter, index) => (
                    <div key={index}>
                        <h3>{filter.name}</h3>
                        {
                            filter.innerArray.map((value, subIndex) => (
                                <div key={subIndex}>
                                    <label>{value}</label>
                                    <Field 
                                        name={filter.name} 
                                        component="input" 
                                        type="checkbox"
                                        value={value}
                                    />
                                </div>
                            ))
                        }
                    </div>
                ))}



                
*/