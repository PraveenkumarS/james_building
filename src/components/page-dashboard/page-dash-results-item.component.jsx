import React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'semantic-ui-react';

const PageDashResultsItem = ({item}) => {
    return (
        <Table.Row>
            <Table.Cell width={4}><Link to={`/buildings/${item.id}`}>{item.name}</Link></Table.Cell>
            <Table.Cell>{item.area}</Table.Cell>
            <Table.Cell>{item.city}</Table.Cell>
            <Table.Cell>{item.country}</Table.Cell>
            <Table.Cell>{item.units}</Table.Cell>
            <Table.Cell>{item.rooms}</Table.Cell>
            <Table.Cell>{item.developer}</Table.Cell>
        </Table.Row>
    );
}

export default PageDashResultsItem;