import styled from 'styled-components';

export const PageForm = styled.div`
    background: ${props => props.theme.white};
    padding: 36px;

    h2 {
        font-size: 20px;
        ${props => props.theme.black};
    }

    .imageDrop{
        max-height: 200px;
        margin-top: 5px;
        marginLeft: 5px;
        cursor: "pointer";
    }

    .dropzone{
        width: 100%;
        text-align: center;
        border: 3px solid #02C8A7;
        padding: 30px;
        border-radius: 6px;
    }

    .droptarget{
        position: relative;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        display: inline-flex;
        margin-bottom: 10px;
        margin: 2%;
        cursor: pointer;
    }

    .droptarget:hover{
      .imageOverlayDiv{
            display: block;
      }
    }

    .imageOverlayDiv{
        position: absolute;
        display: none;
        bottom: -10px;
        height: 40px;
        width: 100%;
        background: #00000073;
        text-align: right;
        border-radius: 0px 0px 8px 8px;
        .trashIcon{
            font-size: 20px;
            color: white;
            margin-right: 10px;
            margin-top: 10px;
            cursor: pointer;
        }
        .trashIcon:hover{
            font-size: 22px;
        }
    }

    .radio-split {
        margin-bottom: 32px;

        .radio-btn {
            display: inline-block;
            width: calc((100% / 3) - 36px);
            margin: 0 18px;

            label {
                text-align: center;
                width: 100%;
                background: ${props => props.theme.grey};
                color: #444;
                border-radius: 5px;
                padding: 10px 10px;
                border: 2px solid ${props => props.theme.medGrey};
                cursor: pointer;
                display: block;
                padding: 24px;

                .icon {
                    color: #000;
                    display: block;
                    margin: auto;
                    font-size: 32px;
                } 
                span {
                    color: ${props => props.theme.black};
                    display: block;
                } 
            }

            input[type="radio"] {
                display: none;
                position: absolute;
                width: 100%;
                appearance: none;
            }

            input[type="radio"]:checked + label {
                background: ${props => props.theme.medGrey};
                animation-name: blink;
                animation-duration: 1s;
                border-color: ${props => props.theme.medGrey};
            }

            input[type="radio"]:checked + label:after {
                background: ${props => props.theme.medGrey};
            }

            input[type="radio"]:checked + label span {
                color: ${props => props.theme.white};
            }

            input[type="radio"]:checked + label .icon {
                color: ${props => props.theme.white};
            }
            
            input[type="radio"]:checked + label:before {
                width: 20px;
                height: 20px;
            }
            
            label:after, label:before {
                display: none;
            }
        }
    }

    .text-input {
        border: 1px solid ${props => props.theme.grey};
        padding: 12px;
        width: 100%;
    }
`


/*
               // &:after, &:before {
                //     content: "";
                //     position: absolute;
                //     right: 11px;
                //     top: 11px;
                //     width: 20px;
                //     height: 20px;
                //     border-radius: 3px;
                //     background: ${props => props.theme.grey};
                // }

                // &:before {
                //     background: transparent;
                //     transition: 0.1s width cubic-bezier(0.075, 0.82, 0.165, 1) 0s, 0.3s height cubic-bezier(0.075, 0.82, 0.165, 2) 0.1s;
                //     z-index: 2;
                //     overflow: hidden;
                //     background-repeat: no-repeat;
                //     background-size: 13px;
                //     background-position: center;
                //     width: 0;
                //     height: 0;
                // }

*/