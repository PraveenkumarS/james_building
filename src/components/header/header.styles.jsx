import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  background: linear-gradient(
    87deg,
    ${props => props.theme.lightBlue},
    ${props => props.theme.darkBlue}
  ) !important;
  width: 100%;
  height: 480px;
`;