import React from 'react';
import { HeaderWrapper } from './header.styles';

const Header = () => {
    return (
        <HeaderWrapper className="header colour" />
    )
}

export default Header