import styled from 'styled-components';

export const ListContainer = styled.div`
    background: ${props => props.theme.white};
    padding: 24px;
    border-radius: 5px;
    box-shadow: 0 0 2rem 0 rgba(136,152,170,.15);

    h2 {
        font-size: 24px;
        color: #000;
        font-weight: 900;
        letter-spacing: 1px;
        padding-bottom: 12px;
        margin-bottom: 16px;
        line-height: 1;
        border-bottom: 1px solid ${props => props.theme.grey};
    }

    .list {
        .item {
            img {
                font-size: 24px;
            }
            .content {
                padding: 5px 0 0 16px !important;

                .header {
                    font-size: 16px;
                    margin-bottom: 4px;
                }
                .description {
                    font-size: 14px;
                }
            }
        }
    } 
`
