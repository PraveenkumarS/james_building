import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from '../../redux/users/user.selectors';
import { Image, List } from 'semantic-ui-react';
import { ListContainer } from './feed.styles';
import { Link } from 'react-router-dom';

const mapState = createStructuredSelector ({
    currentUser: selectCurrentUser
})

const FeedList = ({currentUser}) => {
    return (
        <ListContainer>
            <h2>Feed:</h2>
            <List relaxed='very'>
            <List.Item>
                <Image avatar src={currentUser.photoURL} />
                <List.Content>
                    <List.Header as={Link} to="/">Sandra Simmons</List.Header>
                    <List.Description>
                    Created a {' '}
                    <Link to="/">
                        <b>Blog Post</b>
                    </Link>{' '}
                    just now.
                    </List.Description>
                </List.Content>
                </List.Item>
                <List.Item>
                <Image avatar src={currentUser.photoURL} />
                <List.Content>
                    <List.Header as={Link} to="/">Sandra Simmons</List.Header>
                    <List.Description>
                    Edited a {' '}
                    <Link to="/">
                        <b>Room</b>
                    </Link>{' '}
                    2 hours ago.
                    </List.Description>
                </List.Content>
                </List.Item>
                <List.Item>
                <Image avatar src={currentUser.photoURL} />
                <List.Content>
                    <List.Header as={Link} to="/">Sandra Simmons</List.Header>
                    <List.Description>
                    Uploaded a{' '}
                    <Link to="/">
                        <b>Document</b>
                    </Link>{' '}
                    4 hours ago.
                    </List.Description>
                </List.Content>
                </List.Item>
                <List.Item>
                <Image avatar src={currentUser.photoURL} />
                <List.Content>
                    <List.Header as={Link} to="/">Sandra Simmons</List.Header>
                    <List.Description>
                    Deleted a{' '}
                    <Link to="/">
                        <b>Building</b>
                    </Link>{' '}
                    9 hours ago.
                    </List.Description>
                </List.Content>
                </List.Item>
                <List.Item>
                <Image avatar src={currentUser.photoURL} />
                <List.Content>
                    <List.Header as={Link} to='/'>Sandra Simmons</List.Header>
                    <List.Description>
                    Created an{' '}
                    <Link to="/">
                        <b>Event</b>
                    </Link>{' '}
                    yesterday.
                    </List.Description>
                </List.Content>
                </List.Item>
            </List>
        </ListContainer>
    );
}

export default connect(mapState)(FeedList);