export const loopHelper = (snapshot, fetchedItems) => {
    for (let i = 0; i < snapshot.docs.length; i++) {
      let itemsQuery = {
        ...snapshot.docs[i].data(),
        id: snapshot.docs[i].id
      };
      fetchedItems.push(itemsQuery);
    }
    return fetchedItems;
}


export const objectToArray = (object) => {
  if (object) {
      return Object.entries(object).map(e => Object.assign({}, e[1], {id: e[0]}))
  }
}