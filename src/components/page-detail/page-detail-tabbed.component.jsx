import React from 'react';
import { Tab } from 'semantic-ui-react';
import { TabContainer } from './page-detail-tab.styles';

const TabbedArea = ({panes}) => (
  <TabContainer className="tabbed-area">
    <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
  </TabContainer>
)

export default TabbedArea;