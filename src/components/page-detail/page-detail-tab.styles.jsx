import styled from 'styled-components';

export const TabContainer = styled.div`
    background: ${props => props.theme.white};
    padding: 36px;

    .ui.pointing.secondary.menu {
        .item {
            color: ${props => props.theme.medGrey};
            &.active {
                color: ${props => props.theme.darkBlue};
                border-color: ${props => props.theme.darkBlue};
            }
        }
    }

    .ui.segment {
        box-shadow: none;
        border: none;

        .details {
            .top {
                font-size: 11px;
                text-transform: uppercase;
                letter-spacing: 1px;
                display: block;
                margin-bottom: 12px;
            }
            .bottom {
                font-size: 12px;
                display: block;
            }
        }
    }
    .detail-photos {
        margin-top: 32px;
        
        .photos {
            display: flex;

            img {
                max-height: 100px;
                margin: 0 12px;
            }
        }
    }
`

export const SingleTab = styled.div`
`