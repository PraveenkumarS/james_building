import React, { Component } from 'react';
import ReactMapGL, { Marker, GeolocateControl, Popup, NavigationControl } from 'react-map-gl';
import {
  MapContainer,
  GeolocateContainer
} from './map.styles';
import MapPopup from './map-popup.component';


class Map extends Component {
  state = {
    viewport: {
      latitude: 53.4762,
      longitude: -2.24728,
      width: '100%',
      height: '100%',
      zoom: 13
    },
    selectedItem: null
  };

  render() {
    const { items } = this.props;
    const { selectedItem } = this.state;
    return (
      <MapContainer>
        <ReactMapGL
          {...this.state.viewport}
          mapboxApiAccessToken="pk.eyJ1IjoibmVpZ2hib3VyaG9vZC1saXZpbmciLCJhIjoiY2s1OWlyZmY4MGwzbzNscGYzb3JpZzZzbCJ9.5phDuM8cUqrUbKRbk40R7Q"
          mapStyle="mapbox://styles/mapbox/outdoors-v11"
          onViewportChange={viewport => this.setState({ viewport })}
        >
          <div style={{position: 'absolute', top: '12px', right: '12px'}}>
            <NavigationControl showCompass={false} />
          </div>
          {items && items.map(item => (
            <Marker
              key={item.id}
              latitude={item.latLng.lat}
              longitude={item.latLng.lng}
            >
              <button 
                className="marker-btn"
                onClick={e => {
                  e.preventDefault();
                  this.setState({ selectedItem: item });
                }}
              >
                <img className="marker"
                  alt="neighbourhood marker"
                  src="/assets/images/NBH-Marker2.png"
                />
              </button>
            </Marker>
          ))}
          {selectedItem ? (
            <Popup
              latitude={selectedItem.latLng.lat}
              longitude={selectedItem.latLng.lng}
              onClose={() => {
                this.setState({ selectedItem: null });
              }}
            >
              <MapPopup selectedItem={selectedItem} />
            </Popup>
          ) : null}

          <GeolocateContainer>
            <GeolocateControl
              positionOptions={{ enableHighAccuracy: true }}
              trackUserLocation={true}
            />
          </GeolocateContainer>
        </ReactMapGL>
      </MapContainer>
     
      
    );
  }
}

export default Map;