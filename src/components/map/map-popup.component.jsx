import React from 'react';

const MapPopup = ({selectedItem}) => {
  return (
    <div className="map-popup">
      <img alt='item preview' src={selectedItem.photoURL} />
    </div>
  );
}


export default MapPopup
