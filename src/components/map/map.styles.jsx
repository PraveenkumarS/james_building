import styled from 'styled-components';

export const MapContainer = styled.div`
  width: 400px;
  height: 400px;
  background: #eaeaea;
  position: relative;
  
  .marker-btn {
    background: none;
    border: none;
    cursor: pointer;

    .marker {
      width: 50px;
    }
  }

  
`

export const GeolocateContainer = styled.div`
  position: absolute;
  bottom: 15px;
  right: 0;
  padding: 10px;
`

/*

*/