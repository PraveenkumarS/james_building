import React from 'react';
import { withRouter } from 'react-router-dom';

const NotFound = ({history}) => {
    return (
        <div>
            Not found!
        </div>
    );
}

export default withRouter(NotFound);