const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


// Activity Functions Start //

exports.createBuildingActivity = functions.firestore
  .document('buildings/{buildingId}')
  .onCreate(building => {
    let newBuilding = building.data();
    const activity = {
        type: 'buildingCreated',
        timestamp: admin.firestore.FieldValue.serverTimestamp(),
        buildingId: building.id,
        creatorName: newBuilding.creatorName,
        creatorId: newBuilding.creatorId,
        creatorPhoto: newBuilding.creatorPhoto
    }
    return admin
      .firestore()
      .collection('activity')
      .add(activity)
      .then(docRef => {
        return console.log('Activity created with ID: ', docRef.id);
      })
      .catch(err => {
        return console.log('Error adding activity: ', err);
    });
});

exports.updateBuildingActivity = functions.firestore
  .document('buildings/{buildingId}')
  .onUpdate((building, context) => {
    let updatedBuilding = building.after.data();
    const activity = {
        type: 'buildingUpdated',
        timestamp: admin.firestore.FieldValue.serverTimestamp(),
        buildingId: context.params.buildingId,
        creatorName: updatedBuilding.lastUpdatedByName,
        creatorId: updatedBuilding.lastUpdatedById,
        creatorPhoto: updatedBuilding.lastUpdatedByPhoto
    }
    return admin
      .firestore()
      .collection('activity')
      .add(activity)
      .then(docRef => {
        return console.log('Activity created with ID: ', docRef.id);
      })
      .catch(err => {
        return console.log('Error adding activity: ', err);
    });
});

exports.deleteBuildingActivity = functions.firestore
  .document('buildings/{buildingId}')
  .onDelete(building => {
    let newBuilding = building.data();
    const activity = {
        type: 'buildingDeleted',
        timestamp: admin.firestore.FieldValue.serverTimestamp(),
        buildingId: building.id,
        creatorName: newBuilding.creatorName,
        creatorId: newBuilding.creatorId,
        creatorPhoto: newBuilding.creatorPhoto
    }
    return admin
      .firestore()
      .collection('activity')
      .add(activity)
      .then(docRef => {
        return console.log('Activity created with ID: ', docRef.id);
      })
      .catch(err => {
        return console.log('Error adding activity: ', err);
    });
});




// Activity Functions End //

// firebase deploy --only functions //
// npm start //