import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyDcTJL_FsNUcNK95Uq58I723vw9olZnBCc",
  authDomain: "neighbourhood-15ee9.firebaseapp.com",
  databaseURL: "https://neighbourhood-15ee9.firebaseio.com",
  projectId: "neighbourhood-15ee9",
  storageBucket: "neighbourhood-15ee9.appspot.com",
  messagingSenderId: "193946537294",
  appId: "1:193946537294:web:4fa44e664a96c40930621f",
  measurementId: "G-B7JNK6X73Z"
};

firebase.initializeApp(firebaseConfig);

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      unsubscribe();
      resolve(userAuth);
    }, reject);
  });
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export default firebase;